﻿using System.Windows;
using System.Windows.Input;

namespace AdventureWorks.WorkOrders.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region Properties

        public MainWindowViewModel ViewModel
        {
            get { return this.DataContext as MainWindowViewModel; }
        }

        #endregion

        #region Event Handlers

        private void about_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.ShowAboutBox();
        }

        private void allProducts_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.MouseCursor = Cursors.Wait;
            this.products.ItemsSource = this.ViewModel.GetAllProducts();
            this.ViewModel.MouseCursor = null;
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void search_KeyUp(object sender, KeyEventArgs e)
        {
            this.searchButton.IsEnabled = (this.search.Text.Length > 0) ? true : false;
        }

        #endregion
    }
}
