﻿using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using AdventureWorks.TransactionHistory.Models;

namespace AdventureWorks.TransactionHistory.Views
{
    /// <summary>
    /// Interaction logic for TransactionHistoryView.xaml
    /// </summary>
    public partial class TransactionHistoryView : UserControl
    {
        private DropShadowEffect dropShadow = new DropShadowEffect();

        public TransactionHistoryView()
        {
            this.ViewModel = new TransactionHistoryViewModel(this);
            InitializeComponent();
        }

        public double GraphHeight
        {
            get { return this._graph.ActualHeight; }
        }

        public double GraphWidth
        {
            get { return this._graph.ActualWidth; }
        }

        public Product Product
        {
            get { return this.ViewModel.Product; }
            set { this.ViewModel.Product = value; }
        }

        public TransactionHistoryViewModel ViewModel
        {
            get { return this.DataContext as TransactionHistoryViewModel; }
            private set { this.DataContext = value; }
        }

        public void AddPlotPoint(double x, double y)
        {
            Ellipse plotPoint = new Ellipse();
            plotPoint.Height = 6;
            plotPoint.Width = 6;
            plotPoint.Fill = (Brush)this.Resources["BaseColorBrush"];
            plotPoint.Effect = this.dropShadow;
            plotPoint.SetValue(Canvas.LeftProperty, x - 3);
            plotPoint.SetValue(Canvas.TopProperty, y - 3);
            this._graph.Children.Add(plotPoint);
        }

        public void ClearPlotPoints()
        {
            this._graph.Children.Clear();
        }
    }
}
