﻿using System.Data.Objects;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AdventureWorks.TransactionHistory.Models;

namespace AdventureWorks.TransactionHistory
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private AdventureWorksEntities _context;

        public MainWindow()
        {
            this._context = new AdventureWorksEntities();
            var expression = from p in this._context.Products
                             where p.Name.Contains("Mountain-200")
                             select p;
            this.DataContext = ((ObjectQuery<Product>)expression).Execute(MergeOption.AppendOnly);


            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this._products.SelectedIndex = 0;
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((e.AddedItems.Count > 0) &&
                (e.AddedItems[0] is Product))
            {
                Product selected = (Product)e.AddedItems[0];
                this._history.Product = selected;
            }
        }
    }
}
