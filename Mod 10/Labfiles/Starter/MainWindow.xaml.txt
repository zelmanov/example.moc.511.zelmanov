        <Grid.RowDefinitions>
            <RowDefinition Height="0.4*" />
            <RowDefinition Height="0.6*" />
        </Grid.RowDefinitions>
        <DataGrid x:Name="_products"
                  ItemsSource="{Binding}" />
        <local:TransactionHistoryView x:Name="_history"
                                      Grid.Row="1" />
