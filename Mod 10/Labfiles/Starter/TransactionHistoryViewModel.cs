﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using AdventureWorks.TransactionHistory.Models;

namespace AdventureWorks.TransactionHistory.Views
{
    public class TransactionHistoryViewModel : INotifyPropertyChanged
    {
        private Product _product;
        // TODO: Ex3. Task4. Add shapes by using code. Points field.
        private int maxQuantity;
        private DateTime minDate;
        private DateTime maxDate;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public TransactionHistoryViewModel(TransactionHistoryView historyView)
        {
            this.View = historyView;
        }

        public DateTime MaximumDate
        {
            get
            {
                return this.maxDate;
            }

            private set
            {
                if (value != this.maxDate)
                {
                    this.maxDate = value;
                    this.OnPropertyChanged("MaximumDate");
                }
            }
        }

        public int MaximumQuantity
        {
            get
            {
                return this.maxQuantity;
            }

            set
            {
                if (value != this.maxQuantity)
                {
                    this.maxQuantity = value;
                    this.OnPropertyChanged("MaximumQuantity");
                }
            }
        }

        public DateTime MinimumDate
        {
            get
            {
                return this.minDate;
            }

            set
            {
                if (value != this.minDate)
                {
                    this.minDate = value;
                    this.OnPropertyChanged("MinimumDate");
                }
            }
        }

        // TODO: Ex4. Task1. Add images by using the Image control. Photo property.

        public Product Product
        {
            get
            {
                return this._product;
            }

            set
            {
                if (value != this._product)
                {
                    this._product = value;
                    this.OnPropertyChanged("Product");
                    this.OnPropertyChanged("Photo");
                    // TODO: Ex3. Task4. Add shapes by using code. Product property.
                }
            }
        }

        // TODO: Ex3. Task4. Add shapes by using code. Points property.

        public TransactionHistoryView View { get; private set; }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.EnsureProperty(propertyName);
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        [Conditional("DEBUG")]
        private void EnsureProperty(string propertyName)
        {
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                throw new ArgumentException("Property does not exist.", "propertyName");
            }
        }

        // TODO: Ex3. Task4. Add shapes by using code. ProcessTransactions method.
    }
}
