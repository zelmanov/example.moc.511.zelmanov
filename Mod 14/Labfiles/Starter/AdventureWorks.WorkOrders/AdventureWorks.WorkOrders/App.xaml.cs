﻿using System.Globalization;
using System.Threading;
using System.Windows;
using AdventureWorks.Model;
using AdventureWorks.WorkOrders.Views;

namespace AdventureWorks.WorkOrders
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private MainWindowView window;

        public App() : base()
        {
            string culture = "fr-FR";
            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        }

        /// <summary>
        /// Called when the application starts.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            this.window = new MainWindowView();
         
            // TODO: Ex4. Task1. Update the OnStartup method to use application settings

            // Show the main window.
            var dataService = new DataAccessService(null);
            window.DataContext = new MainWindowViewModel(window, dataService, null);
            
            window.Show();
        }        
    }
}
