﻿using System.Collections.Generic;
using AdventureWorks.WorkOrders.Properties;

namespace AdventureWorks.WorkOrders.Views
{
    public class OptionsViewModel : ViewModelBase
    {
        private List<string> skinNames = new List<string>(2) { "Bureau", "Shiny" };

        public OptionsViewModel() : base()
        {
        }

        // TODO: Ex3. Task1. Complete the OptionsViewModel class.

        public List<string> Skins
        {
            get { return this.skinNames; }
        }                
    }
}
