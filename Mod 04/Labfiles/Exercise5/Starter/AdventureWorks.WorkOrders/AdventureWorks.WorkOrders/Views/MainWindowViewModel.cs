﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using AdventureWorks.Model;
using AdventureWorks.WorkOrders.Commands;

namespace AdventureWorks.WorkOrders.Views
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Member Data

        private IDataAccessService das = null;
        private Cursor mouseCursor;
        private MainWindow view;   

        #endregion

        #region Constructor

        public MainWindowViewModel()
            : this(null)
        {
        }

        public MainWindowViewModel(IDataAccessService dataService)
            : this(dataService, null)
        {
        }

        public MainWindowViewModel(IDataAccessService dataService, MainWindow view)
            : base()
        {
            this.das = dataService;
            this.view = view;
        }

        #endregion

        #region Properties

        public Cursor MouseCursor
        {
            get
            {
                return this.mouseCursor;
            }
            set
            {
                this.mouseCursor = value;
                this.OnPropertyChanged("MouseCursor");
            }
        }

        #endregion

        #region Commands

        #endregion

        #region Public Methods

        public IEnumerable<Product> GetAllProducts()
        {
            return das.GetProductList();
        }

        public void ShowAboutBox()
        {
            AboutWindow about = new AboutWindow();
            about.Owner = Application.Current.MainWindow;
            about.ShowDialog();
        }

        #endregion

        #region HelperMethods

        private void ChangeSkin(object param)
        {
            ResourceDictionary dict = Application.Current.Resources.MergedDictionaries[0];

            if (param.Equals("Shiny"))
            {
                dict.Source = new Uri("pack://application:,,,/AdventureWorks.Resources;component/Themes/ShinyBlue.xaml");
            }
            else
            {
                dict.Source = new Uri("pack://application:,,,/AdventureWorks.Resources;component/Themes/BureauBlue.xaml");
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        protected void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
