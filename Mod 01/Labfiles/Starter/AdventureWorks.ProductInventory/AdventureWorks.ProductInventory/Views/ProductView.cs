﻿using System;
using System.Windows.Forms;
using AdventureWorks.ProductInventory.Presenters;
using System.Windows.Forms.Integration;
using AdventureWorks.TransactionHistory.Views;
using System.IO;

namespace AdventureWorks.ProductInventory.Views
{
    public partial class ProductView : ViewBase
    {
        private ProductViewPresenter _presenter;

        public ProductView()
            : this(null)
        {
        }

        public ProductView(ProductViewPresenter presenter)
        {
            this._presenter = base.SetPresenter(presenter ?? new ProductViewPresenter());
            InitializeComponent();
            this.AddPropertyMappings();
        }

        protected override void InitializeDataBinding()
        {
            this.Text = string.Format(this.Text, this._presenter.Model.Name);
            this.productBindingSource.DataSource = this._presenter.Model;
            this.transactionHistoryView.Product = this._presenter.Model;
        }

        private void AddPropertyMappings()
        {
            this.historyHost.PropertyMap.Add(
                "ForeColor",
                new PropertyTranslator(OnHostForeColorChanged));
            this.historyHost.PropertyMap.Remove("BackgroundImage");
            this.historyHost.PropertyMap.Add(
                "BackgroundImage",
                new PropertyTranslator(OnHostBackgroundImageChanged));
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OnHostBackgroundImageChanged(object h, string propertyName, object value)
        {
            ElementHost host = h as ElementHost;
            TransactionHistoryView history = host.Child as TransactionHistoryView;
            System.Drawing.Bitmap sourceImage = host.BackgroundImage as System.Drawing.Bitmap;
            using (MemoryStream stream = new MemoryStream())
            {
                sourceImage.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                System.Windows.Media.Imaging.BitmapImage targetImage =
                    new System.Windows.Media.Imaging.BitmapImage();
                targetImage.BeginInit();
                targetImage.StreamSource = new MemoryStream(stream.ToArray());
                targetImage.EndInit();
                history.GraphBackground = targetImage;
            }
        }

        private void OnHostForeColorChanged(object h, string propertyName, object value)
        {
            ElementHost host = h as ElementHost;
            System.Drawing.Color sourceColor = (System.Drawing.Color)value;
            TransactionHistoryView history = host.Child as TransactionHistoryView;
            System.Windows.Media.Color targetColor = System.Windows.Media.Color.FromArgb(
                sourceColor.A,
                sourceColor.R,
                sourceColor.G,
                sourceColor.B);
            history.BaseColor = targetColor;
        }
    }
}