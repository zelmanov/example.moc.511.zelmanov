﻿using System;
using System.Windows.Forms;
using AdventureWorks.ProductInventory.Presenters;

namespace AdventureWorks.ProductInventory.Views
{
    public partial class ViewBase : Form
    {
        public ViewBase()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            InitializeDataBinding();
            base.OnLoad(e);
        }

        protected virtual void InitializeDataBinding()
        {
            // No op in base class.
        }

        private PresenterBase _presenter;

        protected T SetPresenter<T>(T presenter)
            where T: PresenterBase
        {
            this._presenter = presenter;
            return (T)this._presenter;
        }

        protected T GetPresenter<T>()
            where T : PresenterBase
        {
            return (T)this._presenter;
        }

        protected override void OnClosed(EventArgs e)
        {
            try
            {
                if (this._presenter != null)
                    this._presenter.Dispose();
            }
            finally
            {
                base.OnClosed(e);
            }
        }
    }
}