﻿using System;
using AdventureWorks.Models;

namespace AdventureWorks.ProductInventory.Presenters
{
    public class ProductViewPresenter : PresenterBase
    {
        private AdventureWorksEntities _context;

        public ProductViewPresenter()
            : this(null, null)
        {
        }

        public ProductViewPresenter(Product model)
            : this(null, model)
        {
        }

        public ProductViewPresenter(AdventureWorksEntities context, Product model)
        {
            this._context = base.SetContext(context ?? new AdventureWorksEntities());
            this.Model = model;
        }

        public Product Model
        {
            get;
            private set;
        }
    }
}