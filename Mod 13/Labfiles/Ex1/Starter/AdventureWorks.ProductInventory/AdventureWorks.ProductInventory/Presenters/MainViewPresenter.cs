﻿using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using AdventureWorks.Models;

namespace AdventureWorks.ProductInventory.Presenters
{
    public class MainViewPresenter : PresenterBase
    {
        private AdventureWorksEntities _context;
        
        public MainViewPresenter()
            : this(null)
        {
        }

        public MainViewPresenter(AdventureWorksEntities context)
        {
            this._context = base.SetContext(context ?? new AdventureWorksEntities());
        }

        public IEnumerable<ProductCategory> CategoryDataSource
        {
            get { return this._context.ProductCategories.Execute(MergeOption.AppendOnly); }
        }

        public string CategoryDisplayMember
        {
            get { return "Name"; }
        }

        public IEnumerable<Product> ProductsDataSource
        {
            get;
            private set;
        }

        private string _searchCriteria;
        public string SearchCriteria
        {
            get { return this._searchCriteria; }
            set
            {
                if (this._searchCriteria != value)
                {
                    this._searchCriteria = value;
                    this.OnPropertyChanged("SearchCriteria");
                }
            }
        }

        public ProductCategory SelectedCategory
        {
            get;
            private set;
        }

        public Product SelectedProduct
        {
            get;
            private set;
        }
        
        public ProductSubcategory SelectedSubcategory
        {
            get;
            private set;
        }

        public IEnumerable<ProductSubcategory> SubcategoryDataSource
        {
            get;
            private set;
        }

        public void Search()
        {
            if (string.IsNullOrWhiteSpace(this.SearchCriteria))
                return;

            // Search for a product
            var criteria = this.SearchCriteria;
            var results =
                from p in this._context.Products
                where
                    p.Name.Contains(criteria) ||
                    p.ProductNumber.Contains(criteria)
                select p;

            // Update the view
             this.ProductsDataSource =
                new List<Product>( // Need to wrap datasource to prevent double enumeration exception
                    ((ObjectQuery<Product>)results).Execute(MergeOption.AppendOnly));
             this.OnPropertyChanged("ProductsDataSource");
        }

        public void SetCategory(ProductCategory category)
        {
            this.SelectedCategory = category;
            this.OnPropertyChanged("SelectedCategory");
            this.SubcategoryDataSource = this.SelectedCategory.ProductSubcategories;
            this.OnPropertyChanged("SubcategoryDataSource");
        }

        public void SetSubcategory(ProductSubcategory subcategory)
        {
            this.SelectedSubcategory = subcategory;
            this.OnPropertyChanged("SelectedSubcategory");
            this.ProductsDataSource = this.SelectedSubcategory.Products;
            this.OnPropertyChanged("ProductsDataSource");
        }

        public void SetProduct(int selectedIndex)
        {
            var source = (IEnumerable<Product>)this.ProductsDataSource;
            this.SelectedProduct = source.Skip(selectedIndex).First();
            this.OnPropertyChanged("SelectedProduct");
        }
    }
}