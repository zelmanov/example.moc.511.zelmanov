//------------------------------------------------------------
// PointCollectionAnimation.cs by Charles Petzold, March 2007
//------------------------------------------------------------
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Petzold.AnimationExtensions
{
    public class PointCollectionAnimation : PointCollectionAnimationBase
    {
        // Predefined collection for the To interpolation value
        // ----------------------------------------------------
        PointCollection ptsTo = new PointCollection();

        // Two predefined collections for destination
        // ------------------------------------------
        PointCollection ptsDst1 = new PointCollection();
        PointCollection ptsDst2 = new PointCollection();

        // Determines which destination collection to use
        // ----------------------------------------------
        bool flip = false;

        // From dependency property and property
        // -------------------------------------
        public static DependencyProperty FromProperty =
            DependencyProperty.Register("From", typeof(PointCollection),
            typeof(PointCollectionAnimation),
            new PropertyMetadata(null));

        public PointCollection From
        {
            set { SetValue(FromProperty, value); }
            get { return (PointCollection)GetValue(FromProperty); }
        }

        // To dependency property and property
        // -----------------------------------
        public static DependencyProperty ToProperty =
            DependencyProperty.Register("To", typeof(PointCollection),
            typeof(PointCollectionAnimation),
            new PropertyMetadata(null));

        public PointCollection To
        {
            set { SetValue(ToProperty, value); }
            get { return (PointCollection)GetValue(ToProperty); }
        }

        // By dependency property and property
        // -----------------------------------
        public static DependencyProperty ByProperty =
            DependencyProperty.Register("By", typeof(PointCollection),
            typeof(PointCollectionAnimation),
            new PropertyMetadata(null));

        public PointCollection By
        {
            set { SetValue(ByProperty, value); }
            get { return (PointCollection)GetValue(ByProperty); }
        }

        // IsAdditive and IsCumulative properties
        // --------------------------------------
        public bool IsAdditive
        {
            set { SetValue(IsAdditiveProperty, value); }
            get { return (bool)GetValue(IsAdditiveProperty); }
        }

        public bool IsCumulative
        {
            set { SetValue(IsCumulativeProperty, value); }
            get { return (bool)GetValue(IsCumulativeProperty); }
        }

        // CreateInstanceCore override required when deriving from Freezable
        // -----------------------------------------------------------------
        protected override Freezable CreateInstanceCore()
        {
            return new PointCollectionAnimation();
        }

        // Required GetCurrentValueCore when deriving from PointCollectionAnimationBase
        // ----------------------------------------------------------------------------
        protected override PointCollection GetCurrentValueCore(
                                                PointCollection defaultOriginValue, 
                                                PointCollection defaultDestinationValue, 
                                                AnimationClock animationClock)
        {
            // Let's hope this doesn't happen too often
            if (animationClock.CurrentProgress == null)
                return null;

            double progress = animationClock.CurrentProgress.Value;
            int count;

            // Set ptsFrom from From or defaultOriginValue
            PointCollection ptsFrom = From ?? defaultOriginValue;

            // Set ptsTo from To, By, or defaultOriginValue
            ptsTo.Clear();

            if (To != null)
            {
                foreach (Point pt in To)
                    ptsTo.Add(pt);
            }
            else if (By != null)
            {
                count = Math.Min(ptsFrom.Count, By.Count);

                for (int i = 0; i < count; i++)
                    ptsTo.Add(new Point(ptsFrom[i].X + By[i].X,
                                        ptsFrom[i].Y + By[i].Y));
            }
            else
            {
                foreach (Point pt in defaultDestinationValue)
                    ptsTo.Add(pt);
            }

            // Choose which destination collection to use
            PointCollection ptsDst = flip ? ptsDst1 : ptsDst2;
            flip = !flip;
            ptsDst.Clear();

            // Interpolate the points
            count = Math.Min(ptsFrom.Count, ptsTo.Count);

            for (int i = 0; i < count; i++)
            {
                ptsDst.Add(new Point((1 - progress) * ptsFrom[i].X + 
                                          progress  * ptsTo[i].X,
                                     (1 - progress) * ptsFrom[i].Y + 
                                          progress  * ptsTo[i].Y));
            }

            // If IsAdditive, add the base values to ptsDst
            if (IsAdditive && From != null && (To != null || By != null))
            {
                count = Math.Min(ptsDst.Count, defaultOriginValue.Count);

                for (int i = 0; i < count; i++)
                {
                    Point pt = ptsDst[i];
                    pt.X += defaultOriginValue[i].X;
                    pt.Y += defaultOriginValue[i].Y;
                    ptsDst[i] = pt;
                }
            }

            // Take account of IsCumulative
            if (IsCumulative && animationClock.CurrentIteration != null)
            {
                int iter = animationClock.CurrentIteration.Value;
                
                for (int i = 0; i < ptsDst.Count; i++)
                {
                    Point pt = ptsDst[i];
                    pt.X += (iter - 1) * (ptsTo[i].X - ptsFrom[i].X);
                    pt.Y += (iter - 1) * (ptsTo[i].Y - ptsFrom[i].Y);
                    ptsDst[i] = pt;
                }
            }

            // Return the PointCollection
            return ptsDst;
        }
    }
}
