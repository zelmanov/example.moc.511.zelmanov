﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AdventureWorks.Models;
using Petzold.AnimationExtensions;

namespace AdventureWorks.TransactionHistory.Views
{
    /// <summary>
    /// Interaction logic for TransactionHistoryView.xaml
    /// </summary>
    public partial class TransactionHistoryView : UserControl
    {
        public static readonly DependencyProperty BaseColorProperty = DependencyProperty.Register(
            "BaseColor",
            typeof(Color),
            typeof(TransactionHistoryView),
            new UIPropertyMetadata(Color.FromRgb(0, 188, 255), BaseColor_Changed));
        public static readonly DependencyProperty GraphBackgroundProperty = DependencyProperty.Register(
            "GraphBackground",
            typeof(BitmapImage),
            typeof(TransactionHistoryView),
            new UIPropertyMetadata(null, GraphBackground_Changed));
        public static readonly DependencyProperty ProductProperty = DependencyProperty.Register(
            "Product", 
            typeof(Product), 
            typeof(TransactionHistoryView), 
            new UIPropertyMetadata(null));
        private const double AnimationStep = 0.1;
        private Storyboard plotPointStory = new Storyboard();
        private Storyboard lineStory = new Storyboard();

        public TransactionHistoryView()
        {
            this.ViewModel = new TransactionHistoryViewModel(this);
            InitializeComponent();
            Binding pointsBinding = new Binding("Transactions");
            this._line.SetBinding(Polyline.PointsProperty, pointsBinding);
        }

        private static void BaseColor_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TransactionHistoryView source = (TransactionHistoryView)d;
            source.Resources["BaseColor"] = (Color)e.NewValue;
        }

        private static void GraphBackground_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TransactionHistoryView source = (TransactionHistoryView)d;
            source.UpdateBackground((BitmapImage)e.NewValue);
        }

        public Color BaseColor
        {
            get { return (Color)GetValue(BaseColorProperty); }
            set { SetValue(BaseColorProperty, value); }
        }

        public BitmapImage GraphBackground
        {
            get { return (BitmapImage)GetValue(GraphBackgroundProperty); }
            set { SetValue(GraphBackgroundProperty, value); }
        }

        public Product Product
        {
            get { return (Product)GetValue(ProductProperty); }
            set { SetValue(ProductProperty, value); }
        }

        internal double GraphHeight
        {
            get { return this._graph.ActualHeight; }
        }

        internal double GraphWidth
        {
            get { return this._graph.ActualWidth; }
        }

        internal TransactionHistoryViewModel ViewModel 
        {
            get { return this.DataContext as TransactionHistoryViewModel; }
            private set { this.DataContext = value; }
        }

        internal void AddPlotPoint(DateTime date, double x, double y)
        {
            // Create the PlotPoint and add it to the graph, but with an 
            // Opacity of 0.0 so we can't see it.
            PlotPoint plot = new PlotPoint(date, x, y)
            {
                Opacity = 0.0
            };
            plot.Click += new RoutedEventHandler(this.PlotPoint_Click);
            this._graph.Children.Add(plot);
            this.CreatePlotPointAnimation(plot);
        }

        internal void ClearPlotPoints()
        {
            foreach (PlotPoint item in this._graph.Children)
            {
                item.Click -= this.PlotPoint_Click;
            }


            this._graph.Children.Clear();
        }

        private void CreateLineAnimation()
        {
            // Create an animation from a flat line to the actual points collection.
            PointCollectionAnimation animation = new PointCollectionAnimation();

            // Add the source and destination points to the animation.
            PointCollection startPoints = new PointCollection(this.ViewModel.Transactions.Count);
            foreach (var item in this.ViewModel.Transactions)
            {
                startPoints.Add(new Point(item.X, this.GraphHeight));
            }

            animation.BeginTime = TimeSpan.FromSeconds(0.0);
            animation.From = startPoints;
            animation.Duration = new Duration(
                TimeSpan.FromSeconds(this.ViewModel.Transactions.Count * AnimationStep));
            animation.To = this.ViewModel.Transactions;

            // Initialize the animation target values.
            Storyboard.SetTarget(animation, this._line);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Points"));

            // Add the animation to the storyboard.
            this.lineStory.Children.Add(animation);
        }

        private void CreatePlotPointAnimation(PlotPoint plot)
        {
            // Create an animation for this PlotPoint and add it to the Storyboard.
            DoubleAnimationUsingKeyFrames animation = new DoubleAnimationUsingKeyFrames();

            // Create the start and end frames.
            TimeSpan startTime = TimeSpan.FromSeconds(this._graph.Children.Count * AnimationStep);
            SplineDoubleKeyFrame startFrame = new SplineDoubleKeyFrame(0.0, startTime);
            TimeSpan endTime = startTime + TimeSpan.FromSeconds(AnimationStep);
            SplineDoubleKeyFrame endFrame = new SplineDoubleKeyFrame(1.0, endTime);

            // Add the frames to the animation.
            animation.KeyFrames.Add(startFrame);
            animation.KeyFrames.Add(endFrame);

            // Initialize the animation target values.
            Storyboard.SetTarget(animation, plot);
            Storyboard.SetTargetProperty(animation, new PropertyPath("(UIElement.Opacity)"));

            // Add the animation to the storyboard.
            this.plotPointStory.Children.Add(animation);
        }

        private void PlotPoint_Click(object sender, RoutedEventArgs e)
        {
            PlotPoint source = (PlotPoint)sender;

            // Update the TransactionsView with the current point.
            this._transactionsView.ViewModel.TransactionDate = source.TransactionDate;
            this._transactionsView.ViewModel.Transactions = from t in this.Product.TransactionHistories
                                                            where t.TransactionDate == source.TransactionDate
                                                            select t;
            this._transactionsView.Visibility = Visibility.Visible;

            Point mousePosition =
                        Mouse.PrimaryDevice.GetPosition(this._transactionsView);
            Storyboard popOut = (Storyboard)this.Resources["PopOut"];
            popOut.AutoReverse = false;

            ((popOut.Children[2]
                        as DoubleAnimationUsingKeyFrames).KeyFrames[0]
                                as EasingDoubleKeyFrame).Value = mousePosition.X;
            ((popOut.Children[3]
                        as DoubleAnimationUsingKeyFrames).KeyFrames[0]
                                as EasingDoubleKeyFrame).Value = mousePosition.Y;

            popOut.Begin();

        }        

        private void UpdateBackground(BitmapImage image)
        {
            ImageBrush brush = (ImageBrush)this._graph.Background;
            brush.ImageSource = image;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this._transactionsView.Visibility = Visibility.Collapsed;
            this.ViewModel.Product = this.Product;

            // The plot points have all been added to the graph now.
            this.CreateLineAnimation();
            this.lineStory.Begin();
            this.plotPointStory.Begin();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.Dispose();
        }

        private void TransactionsView_Close(object sender, RoutedEventArgs e)
        {
            Storyboard popOut = (Storyboard)this.Resources["PopOut"];

            popOut.AutoReverse = true;
            popOut.Begin(this, true);
            popOut.Seek(this, new TimeSpan(0, 0, 0), TimeSeekOrigin.Duration);
        }
    }
}
