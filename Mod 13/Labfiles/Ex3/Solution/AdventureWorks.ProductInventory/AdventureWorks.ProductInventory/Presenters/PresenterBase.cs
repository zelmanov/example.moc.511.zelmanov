﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace AdventureWorks.ProductInventory.Presenters
{
    public abstract class PresenterBase : IDisposable, INotifyPropertyChanged
    {
        private IDisposable _context;

        protected PresenterBase()
        {
        }

        ~PresenterBase() 
        {
            this.Dispose(false);
            Debug.Fail("Dispose not called on Presenter class.");
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._context != null)
                    this._context.Dispose();
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.EnsureProperty(propertyName);
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        protected T SetContext<T>(T context)
            where T: IDisposable
        {
            this._context = context;
            return context;
        }

        private void EnsureProperty(string propertyName)
        {
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                throw new ArgumentException("Property does not exist.", "propertyName");
            }
        }
    }
}