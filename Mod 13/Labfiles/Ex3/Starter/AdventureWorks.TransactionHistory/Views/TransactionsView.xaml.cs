﻿using System.Windows;
using System.Windows.Controls;

namespace AdventureWorks.TransactionHistory.Views
{
	/// <summary>
	/// Interaction logic for TransactionsView.xaml
	/// </summary>
	public partial class TransactionsView : UserControl
	{
        // TODO: Ex3. Task1. Add a RoutedEvent named CloseEvent to the TransactionsView class.

		public TransactionsView()
		{
            this.ViewModel = new TransactionsViewModel();
			this.InitializeComponent();
		}

        // TODO: Ex3. Task1. Add a RoutedEventHandler named Close to the TransactionsView class.

        public TransactionsViewModel ViewModel
        {
            get { return this.DataContext as TransactionsViewModel; }
            set { this.DataContext = value; }
        }

        // TODO: Ex3. Task1. Add a new method named RaiseCloseEvent to the TransactionsView class.        

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.Dispose();
        }      
	}
}