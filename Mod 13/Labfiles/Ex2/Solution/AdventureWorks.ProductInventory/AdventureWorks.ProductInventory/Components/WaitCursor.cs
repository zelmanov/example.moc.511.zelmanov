﻿using System;
using System.Windows.Forms;

namespace AdventureWorks.ProductInventory
{
    public class WaitCursor : IDisposable
    {
        private Form _target;
        private Cursor _prevCursor;

        public WaitCursor(Form target)
        {
            if (target == null)
                throw new ArgumentNullException("target");

            this._target = target;
            this.SetCursor(Cursors.WaitCursor);
        }

        private void SetCursor(Cursor newCursor)
        {
            this._prevCursor = this._target.Cursor;
            this._target.Cursor = newCursor;
        }

        void IDisposable.Dispose()
        {
            this.SetCursor(this._prevCursor);            
        }
    }
}