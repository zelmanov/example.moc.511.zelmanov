﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AdventureWorks.Models;
using Petzold.AnimationExtensions;

namespace AdventureWorks.TransactionHistory.Views
{
    /// <summary>
    /// Interaction logic for TransactionHistoryView.xaml
    /// </summary>
    public partial class TransactionHistoryView : UserControl
    {
        public static readonly DependencyProperty BaseColorProperty = DependencyProperty.Register(
            "BaseColor",
            typeof(Color),
            typeof(TransactionHistoryView),
            new UIPropertyMetadata(Color.FromRgb(0, 188, 255), BaseColor_Changed));
        public static readonly DependencyProperty GraphBackgroundProperty = DependencyProperty.Register(
            "GraphBackground",
            typeof(BitmapImage),
            typeof(TransactionHistoryView),
            new UIPropertyMetadata(null, GraphBackground_Changed));
        public static readonly DependencyProperty ProductProperty = DependencyProperty.Register(
            "Product",
            typeof(Product),
            typeof(TransactionHistoryView),
            new UIPropertyMetadata(null));
        private const double AnimationStep = 0.1;
        private Storyboard plotPointStory = new Storyboard();
        private Storyboard lineStory = new Storyboard();

        public TransactionHistoryView()
        {
            this.ViewModel = new TransactionHistoryViewModel(this);
            InitializeComponent();
            Binding pointsBinding = new Binding("Transactions");
            this._line.SetBinding(Polyline.PointsProperty, pointsBinding);
        }

        private static void BaseColor_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TransactionHistoryView source = (TransactionHistoryView)d;
            source.Resources["BaseColor"] = (Color)e.NewValue;
        }

        private static void GraphBackground_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TransactionHistoryView source = (TransactionHistoryView)d;
            source.UpdateBackground((BitmapImage)e.NewValue);
        }

        public Color BaseColor
        {
            get { return (Color)GetValue(BaseColorProperty); }
            set { SetValue(BaseColorProperty, value); }
        }

        public BitmapImage GraphBackground
        {
            get { return (BitmapImage)GetValue(GraphBackgroundProperty); }
            set { SetValue(GraphBackgroundProperty, value); }
        }

        public Product Product
        {
            get { return (Product)GetValue(ProductProperty); }
            set { SetValue(ProductProperty, value); }
        }

        internal double GraphHeight
        {
            get { return this._graph.ActualHeight; }
        }

        internal double GraphWidth
        {
            get { return this._graph.ActualWidth; }
        }

        internal TransactionHistoryViewModel ViewModel
        {
            get { return this.DataContext as TransactionHistoryViewModel; }
            private set { this.DataContext = value; }
        }

        internal void AddPlotPoint(double x, double y)
        {
            // Create the PlotPoint and add it to the graph, but with an 
            // Opacity of 0.0 so we can't see it.
            PlotPoint plot = new PlotPoint(x, y)
            {
                // TODO: Ex2. Task 3. Update the AddPlotPoint method so that PlotPoint elements are invisible when added. 
                Opacity = 0.0
            };
            this._graph.Children.Add(plot);
            this.CreatePlotPointAnimation(plot);
        }

        internal void ClearPlotPoints()
        {
            this._graph.Children.Clear();
        }

        private void CreatePlotPointAnimation(PlotPoint plot)
        {
            DoubleAnimationUsingKeyFrames animation =
                        new DoubleAnimationUsingKeyFrames();

            TimeSpan startTime = TimeSpan.FromSeconds(this._graph.Children.Count * AnimationStep);
            SplineDoubleKeyFrame startFrame =
                        new SplineDoubleKeyFrame(0.0, startTime);
            TimeSpan endTime =
                        startTime + TimeSpan.FromSeconds(AnimationStep);
            SplineDoubleKeyFrame endFrame =
                        new SplineDoubleKeyFrame(1.0, endTime);

            animation.KeyFrames.Add(startFrame);
            animation.KeyFrames.Add(endFrame);

            Storyboard.SetTarget(animation, plot);
            Storyboard.SetTargetProperty(animation, new PropertyPath("(UIElement.Opacity)"));

            this.plotPointStory.Children.Add(animation);
        }

        private void CreateLineAnimation()
        {
            PointCollectionAnimation animation =
                        new PointCollectionAnimation();

            PointCollection startPoints =
                        new PointCollection(this.ViewModel.Transactions.Count);

            foreach (var item in this.ViewModel.Transactions)
            {
                startPoints.Add(new Point(item.X, this.GraphHeight));
            }

            animation.BeginTime = TimeSpan.FromSeconds(0.0);
            animation.From = startPoints;
            animation.Duration = new Duration(
                        TimeSpan.FromSeconds(
                                    this.ViewModel.Transactions.Count * AnimationStep)
                                        );
            animation.To = this.ViewModel.Transactions;

            Storyboard.SetTarget(animation, this._line);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Points"));

            this.lineStory.Children.Add(animation);
        }

        private void UpdateBackground(BitmapImage image)
        {
            ImageBrush brush = (ImageBrush)this._graph.Background;
            brush.ImageSource = image;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.Product = this.Product;

            // The plot points have all been added to the graph now.
            this.CreateLineAnimation();
            this.lineStory.Begin();
            this.plotPointStory.Begin();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.Dispose();
        }
    }
}
