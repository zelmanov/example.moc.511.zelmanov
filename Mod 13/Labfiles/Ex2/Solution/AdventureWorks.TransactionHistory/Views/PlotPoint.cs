﻿using System.Windows;
using System.Windows.Controls;

namespace AdventureWorks.TransactionHistory.Views
{
    public class PlotPoint : Control
    {
        public static readonly DependencyProperty XProperty = DependencyProperty.Register(
            "X", 
            typeof(double), 
            typeof(PlotPoint), 
            new UIPropertyMetadata(0.0));
        public static readonly DependencyProperty YProperty = DependencyProperty.Register(
            "Y",
            typeof(double),
            typeof(PlotPoint),
            new UIPropertyMetadata(0.0));

        static PlotPoint()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(PlotPoint), 
                new FrameworkPropertyMetadata(typeof(PlotPoint)));
        }

        public PlotPoint() : this(0.0, 0.0)
        {
        }

        public PlotPoint(double xPosition, double yPosition)
        {
            this.DataContext = this;
            this.X = xPosition;
            this.Y = yPosition;
        }

        public double X
        {
            get { return (double)GetValue(XProperty); }
            set { SetValue(XProperty, value); }
        }

        public double Y
        {
            get { return (double)GetValue(YProperty); }
            set { SetValue(YProperty, value); }
        }
    }
}
