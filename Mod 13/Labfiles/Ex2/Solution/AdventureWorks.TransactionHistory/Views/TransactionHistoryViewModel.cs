﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using AdventureWorks.Models;

namespace AdventureWorks.TransactionHistory.Views
{
    public class TransactionHistoryViewModel : ViewModelBase
    {
        private Product _product;
        private PointCollection _points;
        private int maxQuantity;
        private DateTime minDate;
        private DateTime maxDate;

        public TransactionHistoryViewModel(TransactionHistoryView historyView)
        {
            this.View = historyView;
        }

        public DateTime MaximumDate
        {
            get
            {
                return this.maxDate;
            }

            private set
            {
                if (value != this.maxDate)
                {
                    this.maxDate = value;
                    this.OnPropertyChanged("MaximumDate");
                }
            }
        }

        public int MaximumQuantity
        {
            get
            {
                return this.maxQuantity;
            }

            set
            {
                if (value != this.maxQuantity)
                {
                    this.maxQuantity = value;
                    this.OnPropertyChanged("MaximumQuantity");
                }
            }
        }

        public DateTime MinimumDate
        {
            get
            {
                return this.minDate;
            }

            set
            {
                if (value != this.minDate)
                {
                    this.minDate = value;
                    this.OnPropertyChanged("MinimumDate");
                }
            }
        }

        public BitmapImage Photo
        {
            get
            {
                if (null != this.Product)
                {
                    var expression = this.Product.ProductPhotos.FirstOrDefault();
                    if (null != expression)
                    {
                        if (null != expression.ProductPhoto.LargePhoto)
                        {
                            MemoryStream stream = 
                                new MemoryStream(expression.ProductPhoto.LargePhoto);
                            BitmapImage bitmap = new BitmapImage();
                            bitmap.BeginInit();
                            bitmap.StreamSource = stream;
                            bitmap.EndInit();

                            return bitmap;
                        }
                    }
                }

                return null;
            }
        }

        public Product Product
        {
            get
            {
                return this._product;
            }

            set
            {
                if (value != this._product)
                {
                    this._product = value;
                    this.OnPropertyChanged("Product");
                    this.OnPropertyChanged("Photo");
                    this.Transactions = this.ProcessTransactions();
                }
            }
        }

        public PointCollection Transactions
        {
            get
            {
                return this._points;
            }

            private set
            {
                if (value != this._points)
                {
                    this._points = value;
                    this.OnPropertyChanged("Transactions");
                }
            }
        }

        public TransactionHistoryView View { get; private set; }

        private PointCollection ProcessTransactions()
        {
            PointCollection points = new PointCollection(this.Product.TransactionHistories.Count);
            this.View.ClearPlotPoints();

            if ((null != this.Product) &&
                (this.Product.TransactionHistories.Count > 0))
            {
                var expression =
                    from h in this.Product.TransactionHistories
                        .OrderBy(t => t.TransactionDate)
                        .GroupBy(t => t.TransactionDate)
                    select new
                    {
                        TransactionDate = h.Key,
                        TotalQuantity = h.Sum(s => s.Quantity)
                    };

                // Work out the range for the vertical (quantity) 
                // axis and the horizontal (date) axis.
                this.MaximumQuantity = expression.Max(t => t.TotalQuantity);
                this.MinimumDate = expression.Min(t => t.TransactionDate);
                this.MaximumDate = expression.Max(t => t.TransactionDate);

                double horizontalStep = 
                    this.View.GraphWidth / (this.MaximumDate - this.MinimumDate).Days;
                double verticalStep = 
                    this.View.GraphHeight / this.MaximumQuantity;

                // Process the transaction history and add a point for each one.
                foreach (var item in expression)
                {
                    double x = (item.TransactionDate - this.MinimumDate).Days * horizontalStep;
                    double y = (this.MaximumQuantity - item.TotalQuantity) * verticalStep;
                    points.Add(new Point(x, y));
                    this.View.AddPlotPoint(x, y);
                }
            }

            return points;
        }
    }
}
