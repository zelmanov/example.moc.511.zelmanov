//----------------------------------------------------------------
// PointCollectionAnimationBase.cs by Charles Petzold, March 2007
//----------------------------------------------------------------
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Petzold.AnimationExtensions
{
    public abstract class PointCollectionAnimationBase : AnimationTimeline
    {
        // Required TargetPropertyType override
        // ------------------------------------
        public override Type TargetPropertyType
        {
            get { return typeof(PointCollection); }
        }

        // GetCurrentValue override and redefinition
        // -----------------------------------------
        public sealed override object GetCurrentValue(object defaultOriginValue, 
                                                      object defaultDestinationValue, 
                                                      AnimationClock animationClock)
        {
            return GetCurrentValue(defaultOriginValue as PointCollection,
                                   defaultDestinationValue as PointCollection, 
                                   animationClock);
        }

        public PointCollection GetCurrentValue(PointCollection defaultOriginValue, 
                                               PointCollection defaultDestinationValue, 
                                               AnimationClock animationClock)
        {
            return GetCurrentValueCore(defaultOriginValue, 
                                       defaultDestinationValue, 
                                       animationClock);
        }

        // Abstract GetCurrentValue definition
        // -----------------------------------
        protected abstract PointCollection GetCurrentValueCore(
                                                PointCollection defaultOriginValue, 
                                                PointCollection defaultDestinationValue, 
                                                AnimationClock animationClock);
    }
}


