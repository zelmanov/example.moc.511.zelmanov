﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Objects;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using AdventureWorks.Model;
using AdventureWorks.WorkOrders.Commands;

namespace AdventureWorks.WorkOrders.Views
{
    public class WorkOrdersViewModel : ViewModelBase
    {
        private const int RecordsToProcess = 1000;
        private AdventureWorksEntities context = new AdventureWorksEntities();
		private Cursor currentCursor = Cursors.Arrow;
        private Stopwatch timer = new Stopwatch();
        private TimeSpan operationTime = TimeSpan.Zero;
        private RelayCommand ordersCommand;
        private RelayCommand<string> filterCommand;
        private int totalOrders;
        private int currentIndex;
        private bool isBusy;
        private WorkOrder highest;
        private WorkOrder scrapped;
        private int totalQty;

        public WorkOrdersViewModel(WorkOrdersView historyView) : base()
        {
            this.View = historyView;
            this.WorkOrders = new ObservableCollection<WorkOrder>();
        }

        public ICommand AllWorkOrdersCommand
        {
            get
            {
                if (null == this.ordersCommand)
                {
                    //  TODO: Ex2. Task2. Retrieve Work Orders by Using a Dispatcher. AllWorkOrdersCommand.
                }

                return this.ordersCommand;
            }
        }

        public int CurrentIndex
        {
            get
            {
                return this.currentIndex;
            }

            set
            {
                if (this.currentIndex != value)
                {
                    this.currentIndex = value;
                    this.OnPropertyChanged("CurrentIndex");
                }
            }
        }
		
		public Cursor Cursor
		{
			get
			{
				return this.currentCursor;
			}
			
			set
			{
				if (this.currentCursor != value)
				{
					this.currentCursor = value;
					this.OnPropertyChanged("Cursor");
				}
			}
		}

        public TimeSpan ElapsedTime 
        {
            get
            {
                return operationTime;
            }

            set
            {
                if (this.operationTime != value)
                {
                    this.operationTime = value;
                    this.OnPropertyChanged("ElapsedTime");
                }
            }
        }

        public ICommand FilterCommand
        {
            get
            {
                if (null == this.filterCommand)
                {
                    //  TODO: Ex3. Task1. Filter the Work Orders List. FilterCommand.
                }

                return this.filterCommand;
            }
        }

        public bool IsQueryActive 
        {
            get 
            { 
                return this.isBusy; 
            }

            set
            {
                if (this.isBusy != value)
                {
                    this.isBusy = value;
                    this.OnPropertyChanged("IsQueryActive");
                }

                if (true == this.isBusy)
                {
					this.Cursor = Cursors.AppStarting;
                    this.ElapsedTime = TimeSpan.Zero;
                    this.timer.Start();
                }
                else
                {
                    this.timer.Stop();
					this.Cursor = Cursors.Arrow;
                    this.ElapsedTime = this.timer.Elapsed;
                    this.timer.Reset();
                }
            }
        }

        // TODO: Ex3. Task4. Update the Statistics by Using the Parallel Class. Properties.

        public int TotalWorkOrders
        {
            get
            {
                return this.totalOrders;
            }

            set
            {
                if (this.totalOrders != value)
                {
                    this.totalOrders = value;
                    this.OnPropertyChanged("TotalWorkOrders");
                }
            }
        }

        public ObservableCollection<WorkOrder> WorkOrders { get; private set; }

        public WorkOrdersView View { get; set; }

        protected override void Dispose(bool disposing)
        {
            if ((true == disposing) && (null != this.context))
            {
                this.context.Dispose();
            }
        }

        private bool CanExecuteCommands(object param)
        {
            return !this.IsQueryActive;
        }

        private void Reset()
        {
            this.WorkOrders.Clear();
            this.CurrentIndex = 0;
            this.TotalWorkOrders = 0;

            // TODO: Ex3. Task4. Update the Statistics by Using the Parallel Class. Reset.
        }

        private List<WorkOrder> GetData()
        {
            var expression = from w in this.context.WorkOrders
                             select w;
            return ((ObjectQuery<WorkOrder>)expression)
                .Execute(MergeOption.AppendOnly)
                .Take(RecordsToProcess)
                .ToList();
        }

        // TODO: Ex2. Task2. Retrieve Work Orders by Using a Dispatcher. GetWorkOrdersDispatcher.

        // TODO: Ex2. Task3. Retrieve Work Orders by Using the ThreadPool.

        // TODO: Ex2. Task4. Retrieve Work Orders by Using a BackgroundWorker.

        // TODO: Ex3. Task1. Filter the Work Orders List. FilterWorkOrders.

        // TODO: Ex3. Task2. Filter the Work Orders List by Using the Parallel Class.

        // TODO: Ex3. Task4. Update the Statistics by Using the Parallel Class. GetStatistics.
    }
}
