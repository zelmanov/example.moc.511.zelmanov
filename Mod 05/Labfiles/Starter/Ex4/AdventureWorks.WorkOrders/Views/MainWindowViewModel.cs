﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using AdventureWorks.Model;
using AdventureWorks.WorkOrders.Commands;

namespace AdventureWorks.WorkOrders.Views
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Member Data

        private IDataAccessService das = null;
        private bool isBusy;
        private Cursor mouseCursor;
        private IEnumerable<Product> products;
        private MainWindow view;

        private RelayCommand aboutBoxCommand;
        private RelayCommand allProductsCommand;
        private RelayCommand closeCommand;
        private RelayCommand changeSkinCommand;
        private RelayCommand searchProductsCommand;

        #endregion

        #region Constructor

        public MainWindowViewModel()
            : this(null)
        {
        }

        public MainWindowViewModel(IDataAccessService dataService)
            : this(dataService, null)
        {
        }

        public MainWindowViewModel(IDataAccessService dataService, MainWindow view)
            : base()
        {
            this.das = dataService;
            this.view = view;
        }

        #endregion

        #region Properties

        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }
            private set
            {
                this.isBusy = value;
                this.OnPropertyChanged("IsBusy");
            }
        }

        public Cursor MouseCursor
        {
            get
            {
                return this.mouseCursor;
            }
            private set
            {
                this.mouseCursor = value;
                this.OnPropertyChanged("MouseCursor");
            }
        }

        public IEnumerable<Product> Products
        {
            get
            {
                return this.products;
            }
        }

        #endregion

        #region Commands

        public ICommand AboutBoxCommand
        {
            get
            {
                if (this.aboutBoxCommand == null)
                {
                    this.aboutBoxCommand = new RelayCommand(param => this.ShowAboutBox());
                }
                return this.aboutBoxCommand;
            }
        }

        public ICommand AllProductsCommand
        {
            get
            {
                if (this.allProductsCommand == null)
                {
                    this.allProductsCommand = new RelayCommand(param => this.GetAllProducts());
                }
                return this.allProductsCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new RelayCommand(param => Application.Current.Shutdown());
                }
                return this.closeCommand;
            }
        }

        public ICommand ChangeSkinCommand
        {
            get
            {
                if (this.changeSkinCommand == null)
                {
                    this.changeSkinCommand = new RelayCommand(param => this.ChangeSkin(param));
                }
                return this.changeSkinCommand;
            }
        }

        public ICommand SearchProductsCommand
        {
            get
            {
                if (this.searchProductsCommand == null)
                {
                    this.searchProductsCommand = new RelayCommand(param => this.SearchProducts(param));
                }
                return this.searchProductsCommand;
            }
        }

        #endregion

        #region Helper Methods

        private void ChangeSkin(object param)
        {
            ResourceDictionary dict = Application.Current.Resources.MergedDictionaries[0];

            if (param.Equals("Shiny"))
            {
                dict.Source = new Uri("pack://application:,,,/AdventureWorks.Resources;component/Themes/ShinyBlue.xaml");
            }
            else
            {
                dict.Source = new Uri("pack://application:,,,/AdventureWorks.Resources;component/Themes/BureauBlue.xaml");
            }
        }

        private void GetAllProducts()
        {
            this.MouseCursor = Cursors.Wait;
            this.IsBusy = true;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (object sender, DoWorkEventArgs e) =>
            {
                this.products = das.GetProductList();
            };
            worker.RunWorkerCompleted += (object sender, RunWorkerCompletedEventArgs e) =>
            {
                this.IsBusy = false;
                this.MouseCursor = null;
                this.OnPropertyChanged("Products");
            };
            worker.RunWorkerAsync();
        }

        private void SearchProducts(object param)
        {
            this.MouseCursor = Cursors.Wait;
            this.IsBusy = true;

            string[] parameters = ((string)param).Split(':');
            string searchField = parameters[2].Trim();
            BackgroundWorker worker = new BackgroundWorker();

            switch (searchField)
            {
                case "Maximum List Price":
                    decimal maxListPrice = Convert.ToDecimal(parameters[0]);
                    worker.DoWork += (object sender, DoWorkEventArgs e) =>
                    {
                        this.products = das.GetProductList(maxListPrice);
                    };
                    break;
                case "Stock Level":
                    int stockLevel = Convert.ToInt32(parameters[0]);
                    worker.DoWork += (object sender, DoWorkEventArgs e) =>
                    {
                        this.products = das.GetProductList(stockLevel);
                    };
                    break;
                case "Name:":
                default:
                    string productName = parameters[0];
                    worker.DoWork += (object sender, DoWorkEventArgs e) =>
                    {
                        this.products = das.GetProductList(productName);
                    };

                    break;
            }

            worker.RunWorkerCompleted += (object sender, RunWorkerCompletedEventArgs e) =>
            {
                this.IsBusy = false;
                this.MouseCursor = null;
                this.OnPropertyChanged("Products");
            };
            worker.RunWorkerAsync();
        }

        private void ShowAboutBox()
        {
            AboutWindow about = new AboutWindow();
            about.Owner = Application.Current.MainWindow;
            about.ShowDialog();
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        protected void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
