﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.ComponentModel;
using System.Threading;
using AdventureWorks.Tests.Mocks;
using AdventureWorks.WorkOrders.Views;

namespace AdventureWorks.Tests
{
    /// <summary>
    /// Summary description for MainWindowViewModelTest
    /// </summary>
    [TestClass]
    public class MainWindowViewModelTest
    {
        public MainWindowViewModelTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AboutBoxCommandIsNotNull()
        {
            // Arrange: Arrange the test variables.
            var viewModel = new MainWindowViewModel();

            // Act: Act on the subject under test.
            var command = viewModel.AboutBoxCommand;

            // Assert: Assert the assumptions of the test.
            Assert.IsNotNull(command);
        }

        [TestMethod]
        public void AllProductsCommandIsNotNull()
        {
            // Arrange: Arrange the test variables.
            var viewModel = new MainWindowViewModel();

            // Act: Act on the subject under test. 
            var command = viewModel.AllProductsCommand;

            // Assert: Assert the assumptions of the test.
            Assert.IsNotNull(command);
        }

        [TestMethod]
        public void ChangeSkinCommandIsNotNull()
        {
            // Arrange: Arrange the test variables.
            var viewModel = new MainWindowViewModel();

            // Act: Act on the subject under test. 
            var command = viewModel.ChangeSkinCommand;

            // Assert: Assert the assumptions of the test.
            Assert.IsNotNull(command);
        }

        [TestMethod]
        public void CloseCommandIsNotNull()
        {
            // Arrange: Arrange the test variables.
            var viewModel = new MainWindowViewModel();

            // Act: Act on the subject under test. 
            var command = viewModel.CloseCommand;

            // Assert: Assert the assumptions of the test.
            Assert.IsNotNull(command);
        }

        [TestMethod]
        public void SearchProductsCommandIsNotNull()
        {
            // Arrange: Arrange the test variables.
            var viewModel = new MainWindowViewModel();

            // Act: Act on the subject under test. 
            var command = viewModel.SearchProductsCommand;

            // Assert: Assert the assumptions of the test.
            Assert.IsNotNull(command);
        }

        [TestMethod]
        public void GetAllProductsSuccess()
        {
            // Arrange: Arrange the test variables.
            var dataService = new MockDataAccessService();
            var viewModel = new MainWindowViewModel(dataService);
            var command = viewModel.AllProductsCommand;

            // Act: Act on the subject under test.
            WaitForTest(
                viewModel,
                "Products",
                TimeSpan.FromSeconds(1),
                () => command.Execute(null));

            // Assert: Assert the assumptions of the test.
            var expected = 3;
            var actual = viewModel.Products.Count();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SearchProductsByListPriceSuccess()
        {
            // Arrange: Arrange the test variables.
            var dataService = new MockDataAccessService();
            var viewModel = new MainWindowViewModel(dataService);
            var command = viewModel.SearchProductsCommand;
            var commandParameter = "32.0:ComboBoxItem:Maximum List Price";

            // Act: Act on the subject under test.
            WaitForTest(
                viewModel,
                "Products",
                TimeSpan.FromSeconds(1),
                () => command.Execute(commandParameter));

            // Assert: Assert the assumptions of the test.
            var expected = 3;
            var actual = viewModel.Products.Count();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SearchProductsByNameSuccess()
        {
            // Arrange: Arrange the test variables.
            var dataService = new MockDataAccessService();
            var viewModel = new MainWindowViewModel(dataService);
            var command = viewModel.SearchProductsCommand;
            var commandParameter = "Chain:ComboBoxItem:Name";

            // Act: Act on the subject under test.
            WaitForTest(
                viewModel,
                "Products",
                TimeSpan.FromSeconds(1),
                () => command.Execute(commandParameter));

            // Assert: Assert the assumptions of the test.
            var expected = 3;
            var actual = viewModel.Products.Count();
            Assert.AreEqual(expected, actual);
        }

        private void WaitForTest(
            INotifyPropertyChanged target, // INotifyPropertyChanged target instance.
            string propertyName,           // Property to wait for.
            TimeSpan timeout,              // How long to wait before time out.
            Action testAction)             // Test action to perform.
        {
            using (var handle = new ManualResetEventSlim())
            {
                // Event handler for PropertyChanged event.
                target.PropertyChanged += (s, e) =>
                {
                    if (e.PropertyName == propertyName)
                    {
                        // Signal that the event has been raised 
                        // for the property being targeted.
                        handle.Set();
                    }
                };

                // Call the Act part of the Unit Test.
                testAction();

                // Wait for the PropertyChanged event to be raised
                // for the target property.
                if (!handle.Wait(timeout))
                {
                    Assert.Fail("Timeout expired waiting for change notification.");
                }
            }
        }
    }
}