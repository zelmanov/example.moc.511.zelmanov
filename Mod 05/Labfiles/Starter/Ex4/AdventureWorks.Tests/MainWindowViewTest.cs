﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdventureWorks.WorkOrders.Views;
using System.Windows.Automation;
using System.Windows;

namespace AdventureWorks.Tests
{
    /// <summary>
    /// Summary description for MainWindowViewTest
    /// </summary>
    [TestClass]
    public class MainWindowViewTest
    {
        private MainWindow mainWindow;
        private MainWindowViewModel mainWindowViewModel;
        private AutomationElement appWindow;

        public MainWindowViewTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        [TestInitialize]
        public void MyTestInitialize()
        {
            this.mainWindow = new MainWindow();
            this.mainWindowViewModel = new MainWindowViewModel();
            this.mainWindow.DataContext = this.mainWindowViewModel;
            this.mainWindow.Show();

            // Retrieve the application window.
            System.Windows.Automation.Condition windowNameCond =
                new PropertyCondition(AutomationElement.NameProperty,
                    "AdventureWorks Cycles Work Orders");
            this.appWindow = AutomationElement.RootElement.FindFirst(
                TreeScope.Children, windowNameCond);
            Assert.IsNotNull(
                this.appWindow,
                "An element with the name 'AdventureWorks Cycles Work " +
                "Orders' could not be found.");
        }

        [TestCleanup]
        public void MyTestCleanup()
        {
            this.mainWindow.Close();
            System.Windows.Threading.Dispatcher.CurrentDispatcher.InvokeShutdown();
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void PasteMenuItemDisabledTest()
        {
            Clipboard.Clear();

            // Retrieve the Edit menu element.
            AutomationElement editMenu = this.appWindow.FindFirst(
                TreeScope.Descendants,
                new PropertyCondition(
                    AutomationElement.AutomationIdProperty,
                    "EditMenu"));

            // Expand the edit menu to generate menu items.
            ExpandCollapsePattern editMenuPattern = editMenu.GetCurrentPattern(
                ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;

            // Expand the edit menu to make the Paste menu item visible.
            editMenuPattern.Expand();

            // Retrieve the Paste menu item.
            AutomationElement pasteItem = editMenu.FindFirst(
                TreeScope.Descendants,
                new PropertyCondition(
                    AutomationElement.AutomationIdProperty,
                    "EditMenuPasteItem"));

            // Collapse the edit menu.
            editMenuPattern.Collapse();

            // Verify that the paste menu item is disabled.
            bool expectedValue = false;
            bool actualValue = (bool)pasteItem.GetCurrentPropertyValue(
                AutomationElement.IsEnabledProperty);
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void PasteMenuItemEnabledTest()
        {
            Clipboard.SetText("Adventure Works");

            // Retrieve the Edit menu element.
            AutomationElement editMenu = appWindow.FindFirst(
                TreeScope.Descendants,
                new PropertyCondition(
                    AutomationElement.AutomationIdProperty,
                    "EditMenu"));

            // Expand the edit menu to generate menu items.
            ExpandCollapsePattern editMenuPattern = editMenu.GetCurrentPattern(
                ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;

            // Expand the Edit meny to make the Paste menu item visible.
            editMenuPattern.Expand();

            // Retrieve the Paste menu item.
            AutomationElement pasteItem = editMenu.FindFirst(
                TreeScope.Descendants,
                new PropertyCondition(
                    AutomationElement.AutomationIdProperty,
                    "EditMenuPasteItem"));

            // Collapse the Edit menu.
            editMenuPattern.Collapse();

            // Verify that the Paste menu item is disabled.
            bool expectedValue = true;
            bool actualValue = (bool)pasteItem.GetCurrentPropertyValue(
                AutomationElement.IsEnabledProperty);
            Assert.AreEqual(expectedValue, actualValue);
        }
    }
}
