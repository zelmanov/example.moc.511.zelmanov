﻿using System.Windows;
using AdventureWorks.Model;
using AdventureWorks.WorkOrders.Views;
using System.Windows.Threading;

namespace AdventureWorks.WorkOrders
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            this.DispatcherUnhandledException += 
                new DispatcherUnhandledExceptionEventHandler(
                    App_DispatcherUnhandledException);
        }

        void App_DispatcherUnhandledException(
            object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var view = new UnhandledExceptionView();
            var viewModel = new UnhandledExceptionViewModel(e.Exception);
            view.DataContext = viewModel;
            view.Owner = this.MainWindow;
            view.ShowDialog();

            e.Handled = true;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var window = new MainWindow();
            var dataService = new DataAccessService();
            window.DataContext = new MainWindowViewModel(dataService, window);
            window.Show();
        }      
    }
}
