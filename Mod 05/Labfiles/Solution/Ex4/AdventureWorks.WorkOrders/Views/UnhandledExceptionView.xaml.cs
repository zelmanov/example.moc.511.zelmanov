﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AdventureWorks.WorkOrders.Views
{
    /// <summary>
    /// Interaction logic for UnhandledExceptionView.xaml
    /// </summary>
    public partial class UnhandledExceptionView : Window
    {
        public UnhandledExceptionView()
        {
            InitializeComponent();
        }

        private void OnOKClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
