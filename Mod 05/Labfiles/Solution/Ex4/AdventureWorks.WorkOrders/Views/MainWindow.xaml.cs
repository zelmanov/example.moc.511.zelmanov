﻿using System.Windows;
using System;

namespace AdventureWorks.WorkOrders.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindowViewModel ViewModel
        {
            get { return this.DataContext as MainWindowViewModel; }
        }

        #region Event Handlers

        private void search_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            this.searchButton.IsEnabled = (this.search.Text.Length > 0) ? true : false;
        }

        #endregion

        private void OnThrowException(object sender, RoutedEventArgs e)
        {
            throw new ApplicationException("Test exception handling.");
        }
    }
}
