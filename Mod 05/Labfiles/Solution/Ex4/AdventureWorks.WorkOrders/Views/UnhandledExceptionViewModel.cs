﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventureWorks.WorkOrders.Views
{
    public class UnhandledExceptionViewModel
    {
        public UnhandledExceptionViewModel(Exception targetException)
        {
            this.DisplayMessage = targetException.Message;
        }

        public string DisplayMessage { get; set; }
    }
}