﻿using System;
using AdventureWorks.Model;

namespace AdventureWorks.WorkOrders.Views
{
    public class WorkOrderWindowViewModel
    {
        #region Constructor

        public WorkOrderWindowViewModel()
        {
        }

        #endregion

        #region Properties

        public WorkOrder Order { get; set; }

        #endregion
    }
}
