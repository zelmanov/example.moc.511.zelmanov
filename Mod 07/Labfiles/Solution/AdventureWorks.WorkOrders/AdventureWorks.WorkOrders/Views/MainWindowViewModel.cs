﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using AdventureWorks.Model;
using AdventureWorks.WorkOrders.Commands;

namespace AdventureWorks.WorkOrders.Views
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Member Data

        private IDataAccessService das = null;
        private bool isBusy;
        private bool isCreateWorkOrderEnabled;
        private bool isDeleteWorkOrderEnabled;
        private bool isEditWorkOrderEnabled;
        private bool isSearchButtonEnabled;
        private Cursor mouseCursor;
        private Product selectedProduct;
        private WorkOrderViewModel selectedWorkOrder;
        private ICollectionView productView;
        private MainWindow view;
        private ObservableCollection<WorkOrderViewModel> workOrders;
        private IEnumerable<WorkOrderRouting> workOrderRoutings;

        private RelayCommand aboutBoxCommand;
        private RelayCommand allProductsCommand;
        private RelayCommand closeCommand;
        private RelayCommand changeSkinCommand;
        private RelayCommand createWorkOrderCommand;
        private RelayCommand deleteWorkOrderCommand;
        private RelayCommand editWorkOrderCommand;
        private RelayCommand searchProductsCommand;

        private string searchField;
        private string stateField;

        #endregion

        #region Constructor

        public MainWindowViewModel()
            : this(null)
        {
        }

        public MainWindowViewModel(IDataAccessService dataService)
            : this(dataService, null)
        {
        }

        public MainWindowViewModel(IDataAccessService dataService, MainWindow view)
            : base()
        {
            this.das = dataService;
            this.view = view;
            this.workOrders = new ObservableCollection<WorkOrderViewModel>();
        }

        #endregion

        #region Properties
     
        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }
            private set
            {
                this.isBusy = value;
                this.OnPropertyChanged("IsBusy");
            }
        }
      
        public bool IsCreateWorkOrderEnabled
        {
            get
            {
                return this.isCreateWorkOrderEnabled;
            }
            set
            {
                this.isCreateWorkOrderEnabled = value;
                this.OnPropertyChanged("IsCreateWorkOrderEnabled");
            }
        }
     
        public bool IsDeleteWorkOrderEnabled
        {
            get
            {
                return this.isDeleteWorkOrderEnabled;
            }
            set
            {
                this.isDeleteWorkOrderEnabled = value;
                this.OnPropertyChanged("IsDeleteWorkOrderEnabled");
            }
        }
    
        public bool IsEditWorkOrderEnabled
        {
            get
            {
                return this.isEditWorkOrderEnabled;
            }
            set
            {
                this.isEditWorkOrderEnabled = value;
                this.OnPropertyChanged("IsEditWorkOrderEnabled");
            }
        }
    
        public bool IsSearchButtonEnabled
        {
            get
            {
                return this.isSearchButtonEnabled;
            }
            set
            {
                this.isSearchButtonEnabled = value;
                this.OnPropertyChanged("IsSearchButtonEnabled");
            }
        }
     
        public Cursor MouseCursor
        {
            get
            {
                return this.mouseCursor;
            }
            private set
            {
                this.mouseCursor = value;
                this.OnPropertyChanged("MouseCursor");
            }
        }

        public Product SelectedProduct
        {
            get
            {
                return this.selectedProduct;
            }
            set
            {
                this.selectedProduct = value;
                this.GetWorkOrders(((Product)selectedProduct).ProductID);
            }
        }

        public WorkOrderViewModel SelectedWorkOrder
        {
            get
            {
                return this.selectedWorkOrder;
            }
            set
            {
                this.selectedWorkOrder = value;
                this.GetWorkOrderRoutings(((WorkOrderViewModel)selectedWorkOrder).WorkOrderID);
            }
        }

        public ICollectionView ProductView
        {
            get
            {
                return this.productView;
            }
        }

        public WorkOrderViewModel WorkOrder
        {
            get
            {
                return this.selectedWorkOrder;
            }
            set
            {
                this.selectedWorkOrder = value;
                this.EditWorkOrder();
            }
        }

        public IEnumerable<WorkOrderViewModel> WorkOrders
        {
            get
            {
                return this.workOrders;
            }
        }

        public IEnumerable<WorkOrderRouting> WorkOrderRoutings
        {
            get
            {
                return this.workOrderRoutings;
            }
        }

        #endregion

        #region Protected Methods

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            this.WorkOrderViewModelDispose();
            if (this.das != null)
            {
                this.das.ServiceDispose();
            }
        }

        #endregion

        #region Commands

        public ICommand AboutBoxCommand
        {
            get
            {
                if (this.aboutBoxCommand == null)
                {
                    this.aboutBoxCommand = new RelayCommand(param => this.ShowAboutBox());
                }
                return this.aboutBoxCommand;
            }
        }

        public ICommand AllProductsCommand
        {
            get
            {
                if (this.allProductsCommand == null)
                {
                    this.allProductsCommand = new RelayCommand(param => this.GetAllProducts());
                }
                return this.allProductsCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new RelayCommand(param => Application.Current.Shutdown());
                }
                return this.closeCommand;
            }
        }

        public ICommand ChangeSkinCommand
        {
            get
            {
                if (this.changeSkinCommand == null)
                {
                    this.changeSkinCommand = new RelayCommand(param => this.ChangeSkin(param));
                }
                return this.changeSkinCommand;
            }
        }

        public ICommand CreateWorkOrderCommand
        {
            get
            {
                if (this.createWorkOrderCommand == null)
                {
                    this.createWorkOrderCommand = new RelayCommand(param => this.CreateWorkOrder(param));
                }
                return this.createWorkOrderCommand;
            }
        }

        public ICommand DeleteWorkOrderCommand
        {
            get
            {
                if (this.deleteWorkOrderCommand == null)
                {
                    this.deleteWorkOrderCommand = new RelayCommand(param => this.DeleteWorkOrder());
                }
                return this.deleteWorkOrderCommand;
            }
        }

        public ICommand EditWorkOrderCommand
        {
            get
            {
                if (this.editWorkOrderCommand == null)
                {
                    this.editWorkOrderCommand = new RelayCommand(param => this.EditWorkOrder());
                }
                return this.editWorkOrderCommand;
            }
        }

        public ICommand SearchProductsCommand
        {
            get
            {
                if (this.searchProductsCommand == null)
                {
                    this.searchProductsCommand = new RelayCommand(param => this.SearchProducts(param), param => this.isSearchButtonEnabled);
                }
                return this.searchProductsCommand;
            }
        }

        #endregion

        #region Helper Methods
       
        protected virtual void ChangeSkin(object param)
        {
            ResourceDictionary dict = Application.Current.Resources.MergedDictionaries[0];

            if (param.Equals("Shiny"))
            {
                dict.Source = new Uri("pack://application:,,,/AdventureWorks.Resources;component/Themes/ShinyBlue.xaml");
            }
            else
            {
                dict.Source = new Uri("pack://application:,,,/AdventureWorks.Resources;component/Themes/BureauBlue.xaml");
            }
        }
       
        private void CreateWorkOrder(object param)
        {
            WorkOrder workOrder = new WorkOrder()
            {
                OrderQty = 1,
                StockedQty = 1,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                DueDate = DateTime.Now
            };

            WorkOrderWindow order = new WorkOrderWindow();
            order.Owner = Application.Current.MainWindow;
            order.ViewModel.Order = workOrder;
            order.ViewModel.Order.ProductID = ((Product)param).ProductID;

            if (order.ShowDialog().Value == true)
            {
                this.MouseCursor = Cursors.Wait;
                this.workOrders.Clear();

                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += CreateWorkOrder_OnDoWork;
                worker.RunWorkerCompleted += CreateWorkOrder_OnRunWorkerCompleted;
                worker.RunWorkerAsync(workOrder);
            }
        }

        protected virtual void CreateWorkOrder_OnDoWork(object sender, DoWorkEventArgs e)
        {
            IEnumerable<WorkOrder> orders = this.das.CreateWorkOrder((WorkOrder)e.Argument);
            e.Result = orders;
        }

        protected virtual void CreateWorkOrder_OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
                foreach (WorkOrder wOrder in (IEnumerable<WorkOrder>)e.Result)
                    {
                        WorkOrderViewModel vmOrder = new WorkOrderViewModel()
                        {
                            WorkOrderID = wOrder.WorkOrderID,
                            ProductID = wOrder.ProductID,
                            OrderQty = wOrder.OrderQty,
                            StockedQty = wOrder.StockedQty,
                            ScrappedQty = wOrder.ScrappedQty,
                            StartDate = wOrder.StartDate,
                            EndDate = wOrder.EndDate,
                            DueDate = wOrder.DueDate,
                            ScrapReasonID = wOrder.ScrapReasonID,
                            ModifiedDate = wOrder.ModifiedDate
                        };
                        
                        this.workOrders.Add(vmOrder);
                           
                    }

                this.MouseCursor = null;
                this.OnPropertyChanged("WorkOrders");
        }

        protected void DeleteWorkOrder()
        {
            StyledMessageBoxView messageBox = new StyledMessageBoxView();
            messageBox.Owner = Application.Current.MainWindow;
            messageBox.Title = "Delete work order";
            messageBox.ViewModel.Message = "Are you sure you want to delete this work order?";

            if (messageBox.ShowDialog().Value == true)
            {
                this.MouseCursor = Cursors.Wait;
                this.workOrders.Clear();

                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += DeleteWorkOrder_OnDoWork;
                worker.RunWorkerCompleted += DeleteWorkOrder_OnRunWorkerCompleted;
                worker.RunWorkerAsync();
            }
            
            messageBox.ViewModel.Dispose();
        }

        protected void DeleteWorkOrder_OnDoWork(object sender, DoWorkEventArgs e)
        {
            IEnumerable<WorkOrder> orders = das.DeleteWorkOrder((int)e.Argument);
            this.WorkOrderViewModelDispose();
            e.Result = orders;

        }

        protected void DeleteWorkOrder_OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            foreach (WorkOrder order in (IEnumerable<WorkOrder>)e.Result)
            {
                WorkOrderViewModel vmOrder = new WorkOrderViewModel()
                {
                    WorkOrderID = order.WorkOrderID,
                    ProductID = order.ProductID,
                    OrderQty = order.OrderQty,
                    StockedQty = order.StockedQty,
                    ScrappedQty = order.ScrappedQty,
                    StartDate = order.StartDate,
                    EndDate = order.EndDate,
                    DueDate = order.DueDate,
                    ScrapReasonID = order.ScrapReasonID,
                    ModifiedDate = order.ModifiedDate
                };

                this.workOrders.Add(vmOrder);
            }

            this.MouseCursor = null;
            this.OnPropertyChanged("WorkOrders");

        }

        protected void EditWorkOrder()
        {
            WorkOrder workOrder = new WorkOrder()
            {
                WorkOrderID = this.selectedWorkOrder.WorkOrderID,
                ProductID = this.selectedWorkOrder.ProductID,
                ScrappedQty = this.selectedWorkOrder.ScrappedQty,
                ScrapReasonID = this.selectedWorkOrder.ScrapReasonID,
                OrderQty = this.selectedWorkOrder.OrderQty,
                StockedQty = this.selectedWorkOrder.StockedQty,
                StartDate = this.selectedWorkOrder.StartDate,
                EndDate = this.selectedWorkOrder.EndDate,
                DueDate = this.selectedWorkOrder.DueDate
            };

            WorkOrderWindow order = new WorkOrderWindow();
            order.Owner = Application.Current.MainWindow;
            order.ViewModel.Order = workOrder;

            if (order.ShowDialog().Value == true)
            {
                this.MouseCursor = Cursors.Wait;
                this.workOrders.Clear();

                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += EditWorkOrder_OnDoWork;
                worker.RunWorkerCompleted += EditWorkOrder_OnRunWorkerCompleted;
                worker.RunWorkerAsync(workOrder);
            }
        }

        protected virtual void EditWorkOrder_OnDoWork(object sender, DoWorkEventArgs e)
        {
            IEnumerable<WorkOrder> orders = this.das.UpdateWorkOrder((WorkOrder)e.Argument);
            e.Result = orders;
            this.WorkOrderViewModelDispose();
        }

        protected virtual void EditWorkOrder_OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            foreach (WorkOrder wOrder in (IEnumerable<WorkOrder>)e.Result)
                    {
                        WorkOrderViewModel vmOrder = new WorkOrderViewModel()
                        {
                            WorkOrderID = wOrder.WorkOrderID,
                            ProductID = wOrder.ProductID,
                            OrderQty = wOrder.OrderQty,
                            StockedQty = wOrder.StockedQty,
                            ScrappedQty = wOrder.ScrappedQty,
                            StartDate = wOrder.StartDate,
                            EndDate = wOrder.EndDate,
                            DueDate = wOrder.DueDate,
                            ScrapReasonID = wOrder.ScrapReasonID,
                            ModifiedDate = wOrder.ModifiedDate
                        };

                        this.workOrders.Add(vmOrder);
                    }
            this.MouseCursor = null;
            this.OnPropertyChanged("WorkOrders");

        }
           
        protected void GetAllProducts()
        {
            this.MouseCursor = Cursors.Wait;
            this.IsBusy = true;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (object sender, DoWorkEventArgs e) =>
            {
                this.productView = CollectionViewSource.GetDefaultView(das.GetProductList());
            };
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GetAllProductsWorker_RunWorkerCompleted);
            worker.RunWorkerAsync();
        }
        
        protected virtual void GetAllProductsWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.IsBusy = false;
            this.MouseCursor = null;
            this.OnPropertyChanged("ProductView");
        }

        protected void GetWorkOrders(int id)
        {
            this.MouseCursor = Cursors.Wait;
            this.IsBusy = true;
            this.workOrders.Clear();

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += GetWorkOrders_OnDoWork;
            worker.RunWorkerCompleted += GetWorkOrders_OnRunWorkerCompleted;
            worker.RunWorkerAsync(id);
        }

        protected virtual void GetWorkOrders_OnDoWork(object sender, DoWorkEventArgs e)
        {
            IEnumerable<WorkOrder> orders = das.GetWorkOrders((int)e.Argument);
            e.Result = orders;

        }
        
        protected virtual void GetWorkOrders_OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            foreach (WorkOrder order in (IEnumerable<WorkOrder>) e.Result)
                {
                    WorkOrderViewModel vmOrder = new WorkOrderViewModel()
                    {
                        WorkOrderID = order.WorkOrderID,
                        ProductID = order.ProductID,
                        OrderQty = order.OrderQty,
                        StockedQty = order.StockedQty,
                        ScrappedQty = order.ScrappedQty,
                        StartDate = order.StartDate,
                        EndDate = order.EndDate,
                        DueDate = order.DueDate,
                        ScrapReasonID = order.ScrapReasonID,
                        ModifiedDate = order.ModifiedDate
                    };

                    Dispatcher.CurrentDispatcher.BeginInvoke(
                             DispatcherPriority.Background,
                                  new Action(() => this.workOrders.Add(vmOrder)));
                }
            
            this.IsBusy = false;
            this.MouseCursor = null;
            this.OnPropertyChanged("WorkOrders");
        }
        
        protected void GetWorkOrderRoutings(int id)
        {
            this.MouseCursor = Cursors.Wait;
            this.IsBusy = true;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (object sender, DoWorkEventArgs e) =>
            {
                this.workOrderRoutings = das.GetWorkOrderRouting(id);
            };
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GetWorkOrderRoutingsWorker_RunWorkerCompleted);
            worker.RunWorkerAsync();
        }
       
        protected virtual void GetWorkOrderRoutingsWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.IsBusy = false;
            this.MouseCursor = null;
            this.OnPropertyChanged("WorkOrderRoutings");
        }
        
        private string GetSearchField(object param)
        {
            string[] parameters = ((string)param).Split(':');
            return parameters[2].Trim();
        }
        
        private string GetStateField(object param)
        {
            string[] parameters = ((string)param).Split(':');
            return parameters[0]; 
        }
          
        protected void SearchProducts(object param)
        {
            this.MouseCursor = Cursors.Wait;
         
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += SearchProducts_OnDoWork;
            worker.RunWorkerCompleted += SearchProducts_OnRunWorkerCompleted;
            worker.RunWorkerAsync();
        }

        protected virtual void SearchProducts_OnDoWork(object sender, DoWorkEventArgs e)
        {
            IEnumerable<Product> products = this.das.GetProductList();
            e.Result = products;  
            
        }

        protected virtual void SearchProducts_OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.productView = CollectionViewSource.GetDefaultView(e.Result); //might need cast

            switch (this.searchField)
            {
                case  "Maximum List Price":
                    decimal maxListPrice = Convert.ToDecimal(this.stateField);
                    this.productView.Filter = (o) =>
                    {
                        var product = o as Product;
                        return product.ListPrice <= maxListPrice;
                    };
                    break;
                case "Stock Level":
                    int stockLevel = Convert.ToInt32(this.stateField);
                    this.productView.Filter = (o) =>
                    {
                        var product = o as Product;
                        return product.SafetyStockLevel <= stockLevel;
                    };
                    break;
                case "Name:":
                default:
                    string productName = this.stateField;
                    this.productView.Filter = delegate(object item)
                    {
                        Product product = item as Product;
                        return product.Name.Contains(productName);
                    };
                    break;
            }
            
            
            
            
            
            this.IsBusy = false;
            this.MouseCursor = null;
            this.OnPropertyChanged("ProductView");
        }
   
        protected virtual void ShowAboutBox()
        {
            AboutWindow about = new AboutWindow();
            about.Owner = Application.Current.MainWindow;
            about.ShowDialog();
        }

        private void WorkOrderViewModelDispose()
        {
            foreach (WorkOrderViewModel viewModel in this.workOrders)
            {
                viewModel.Dispose();
            }
        }

        #endregion
    }
}
