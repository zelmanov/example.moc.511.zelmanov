﻿using System.Windows;

namespace AdventureWorks.WorkOrders.Views
{
    /// <summary>
    /// Interaction logic for StyledMessageBoxView.xaml
    /// </summary>
    public partial class StyledMessageBoxView : Window
    {
        public StyledMessageBoxView()
        {
            this.ViewModel = new StyledMessageBoxViewModel();
            InitializeComponent();
        }

        public StyledMessageBoxViewModel ViewModel
        {
            get { return this.DataContext as StyledMessageBoxViewModel; }
            set { this.DataContext = value; }
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
