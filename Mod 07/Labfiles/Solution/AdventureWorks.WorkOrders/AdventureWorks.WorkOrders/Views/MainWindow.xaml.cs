﻿using System.Windows;
using System.Windows.Controls;
using AdventureWorks.Model;

namespace AdventureWorks.WorkOrders.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindowViewModel ViewModel
        {
            get { return this.DataContext as MainWindowViewModel; }
        }

        #region Protected Methods

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            this.ViewModel.Dispose();
        }

        #endregion

        #region Event Handlers

        private void products_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.ViewModel.SelectedProduct = this.products.SelectedItem as Product;   
        }
        
        private void search_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            this.ViewModel.IsSearchButtonEnabled = (this.search.Text.Length > 0) ? true : false;
        }

        private void workOrders_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.ViewModel.WorkOrder = ((ListViewItem)sender).Content as WorkOrderViewModel;
            e.Handled = true;
        }

        private void workOrders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.ViewModel.SelectedWorkOrder = this.workOrders.SelectedItem as WorkOrderViewModel; 
        }

        #endregion
    }
}
