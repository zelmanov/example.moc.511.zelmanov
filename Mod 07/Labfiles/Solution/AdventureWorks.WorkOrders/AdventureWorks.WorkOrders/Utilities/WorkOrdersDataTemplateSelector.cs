﻿using System;
using System.Windows;
using System.Windows.Controls;
using AdventureWorks.WorkOrders.Views;

namespace AdventureWorks.WorkOrders.Utilities
{
    public class WorkOrdersDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate SlackTemplate { get; set; }
        public DataTemplate NoSlackTemplate { get; set; }


        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item != null)
            {
                if (((WorkOrderViewModel)item).Slack > TimeSpan.Zero)
                {
                    return this.SlackTemplate;
                }
                else
                {
                    return this.NoSlackTemplate;
                }
            }
            return null;
        }
    }
}
