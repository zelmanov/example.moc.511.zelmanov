 <GroupStyle>
                                <GroupStyle.ContainerStyle>
                                    <Style TargetType="{x:Type GroupItem}">
                                        <Setter Property="Margin"
                                                Value="0,0,0,5" />
                                        <Setter Property="Template">
                                            <Setter.Value>
                                                <ControlTemplate TargetType="{x:Type GroupItem}">
                                                    <Expander IsExpanded="True">
                                                        <Expander.Header>
                                                            <DockPanel>
                                                                <TextBlock Text="Start Date: " />
                                                                <TextBlock Text="{Binding Path=Name}" />
                                                                <TextBlock Text=", Items: " />
                                                                <TextBlock Text="{Binding ItemCount}" />
                                                            </DockPanel>
                                                        </Expander.Header>
                                                        <Expander.Content>
                                                            <ItemsPresenter />
                                                        </Expander.Content>
                                                    </Expander>
                                                </ControlTemplate>
                                            </Setter.Value>
                                        </Setter>
                                    </Style>
                                </GroupStyle.ContainerStyle>
                            </GroupStyle>