﻿using System;
using System.ComponentModel;

namespace AdventureWorks.Model
{
    public partial class WorkOrder : IDataErrorInfo
    {
        #region IDataErrorInfo Members

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string name]
        {
            get
            {
                string result = null;

                if (name == "OrderQty")
                {
                    if (this._OrderQty <= 0)
                    {
                        result = "Order Quantity cannot be less than or equal to 0.";
                    }
                }

                if (name == "StockedQty")
                {
                    if (this._StockedQty <= 0)
                    {
                        result = "Stock Quantity cannot be less than or equal to 0.";
                    }
                    else if (this._StockedQty != this._OrderQty)
                    {
                        result = "Stock Quantity must be the same value as Order Quantity.";
                    }
                }

                if (name == "StartDate")
                {
                    if (this._StartDate < DateTime.Today)
                    {
                        result = string.Format("Start Date cannot be before {0}.", string.Format("{0:MM/dd/yyyy}", DateTime.Today));
                    }
                }

                if (name == "EndDate")
                {
                    if (this._EndDate < this._StartDate)
                    {
                        result = string.Format("End Date cannot be before {0}.", string.Format("{0:MM/dd/yyyy}", this._StartDate));
                    }
                }    
                  
                if (name == "DueDate")
                {
                    if (this._DueDate < this._StartDate)
                    {
                        result = string.Format("Due Date cannot be before {0}.", string.Format("{0:MM/dd/yyyy}", this._StartDate));
                    }
                }

                return result;
            }
        }

        #endregion
    }
}
