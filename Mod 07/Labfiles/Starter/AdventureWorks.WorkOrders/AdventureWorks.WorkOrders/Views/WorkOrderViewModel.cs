﻿using System;

namespace AdventureWorks.WorkOrders.Views
{
    public class WorkOrderViewModel : ViewModelBase
    {
        #region Member Data

        private int workOrderID;
        private int productID;
        private int orderQty;
        private int stockedQty;
        private short scrappedQty;
        private DateTime startDate;
        private DateTime? endDate;
        private DateTime dueDate;
        private short? scrapReasonID;
        private DateTime modifiedDate;

        #endregion

        #region Constructor

        public WorkOrderViewModel()
        {
        }

        #endregion

        #region Properties

        public DateTime DueDate
        {
            get
            {
                return this.dueDate;
            }
            set
            {
                this.dueDate = value;
                this.OnPropertyChanged("DueDate");
            }
        }

        public DateTime? EndDate
        {
            get
            {
                return this.endDate;
            }
            set
            {
                this.endDate = value;
                this.OnPropertyChanged("EndDate");
            }
        }

        public string FormattedDueDate
        {
            get
            {
                return string.Format("{0:MM/dd/yyyy}", this.dueDate);
            }
        }

        public string FormattedEndDate
        {
            get
            {
                return string.Format("{0:MM/dd/yyyy}", this.endDate);
            }
        }

        public string FormattedStartDate
        {
            get
            {
                return string.Format("{0:MM/dd/yyyy}", this.startDate);
            }
        }

        public DateTime ModifiedDate
        {
            get
            {
                return this.modifiedDate;
            }
            set
            {
                this.modifiedDate = value;
                this.OnPropertyChanged("ModifiedDate");
            }
        }

        public int OrderQty
        {
            get
            {
                return this.orderQty;
            }
            set
            {
                this.orderQty = value;
                this.OnPropertyChanged("OrderQty");
            }
        }

        public int ProductID
        {
            get
            {
                return this.productID;
            }
            set
            {
                this.productID = value;
                this.OnPropertyChanged("ProductID");
            }
        }

        public short? ScrapReasonID
        {
            get
            {
                return this.scrapReasonID;
            }
            set
            {
                this.scrapReasonID = value;
                this.OnPropertyChanged("ScrapReasonID");
            }
        }

        public short ScrappedQty
        {
            get
            {
                return this.scrappedQty;
            }
            set
            {
                this.scrappedQty = value;
                this.OnPropertyChanged("ScrappedQty");
            }
        }

        public TimeSpan? Slack
        {
            get
            {
                return this.dueDate - this.endDate;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return this.startDate;
            }
            set
            {
                this.startDate = value;
                this.OnPropertyChanged("StartDate");
            }
        }

        public int StockedQty
        {
            get
            {
                return this.stockedQty;
            }
            set
            {
                this.stockedQty = value;
                this.OnPropertyChanged("StockedQty");
            }
        }


        public int WorkOrderID
        {
            get
            {
                return this.workOrderID;
            }
            set
            {
                this.workOrderID = value;
                this.OnPropertyChanged("WorkOrderID");
            }
        }

        #endregion

        #region Protected Methods

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        #endregion
    }
}
