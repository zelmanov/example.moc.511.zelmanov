﻿using System.Windows;
using AdventureWorks.WorkOrders.Utilities;

namespace AdventureWorks.WorkOrders.Views
{
    /// <summary>
    /// Interaction logic for WorkOrderWindow.xaml
    /// </summary>
    public partial class WorkOrderWindow : Window
    {
        public WorkOrderWindow()
        {
            this.ViewModel = new WorkOrderWindowViewModel();
            InitializeComponent();
        }

        public WorkOrderWindowViewModel ViewModel
        {
            get { return this.DataContext as WorkOrderWindowViewModel; }
            set { this.DataContext = value; }
        }

        #region Event Handlers

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            if (UserInterfaceUtilities.ValidateVisualTree(this) == true)
            {
                this.DialogResult = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.orderDetails.DataContext = this.ViewModel.Order;
        }

        #endregion
    }
}
