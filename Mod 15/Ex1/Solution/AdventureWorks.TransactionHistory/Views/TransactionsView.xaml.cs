﻿using System.Windows;
using System.Windows.Controls;

namespace AdventureWorks.TransactionHistory.Views
{
	/// <summary>
	/// Interaction logic for TransactionsView.xaml
	/// </summary>
	public partial class TransactionsView : UserControl
	{
        public static readonly RoutedEvent CloseEvent = EventManager.RegisterRoutedEvent(
            "Close",
            RoutingStrategy.Bubble,
            typeof(RoutedEventHandler),
            typeof(TransactionsView));

		public TransactionsView()
		{
            this.ViewModel = new TransactionsViewModel();
			this.InitializeComponent();
		}

        public event RoutedEventHandler Close
        {
            add { this.AddHandler(CloseEvent, value); }
            remove { this.RemoveHandler(CloseEvent, value); }
        }

        public TransactionsViewModel ViewModel
        {
            get { return this.DataContext as TransactionsViewModel; }
            set { this.DataContext = value; }
        }

        private void RaiseCloseEvent()
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(TransactionsView.CloseEvent);
            this.RaiseEvent(eventArgs);
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.RaiseCloseEvent();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.Dispose();
        }
	}
}