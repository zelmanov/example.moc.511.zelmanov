﻿using System;
using System.Collections.Generic;

namespace AdventureWorks.TransactionHistory.Views
{
    public class TransactionsViewModel : ViewModelBase
    {
        private DateTime _transactionDate;
        private IEnumerable<Models.TransactionHistory> _transactions;

        public TransactionsViewModel() : base()
        {
        }

        public DateTime TransactionDate
        {
            get
            {
                return this._transactionDate;
            }

            set
            {
                if (value != this._transactionDate)
                {
                    this._transactionDate = value;
                    this.OnPropertyChanged("TransactionDate");
                }
            }
        }

        public IEnumerable<Models.TransactionHistory> Transactions
        {
            get
            {
                return this._transactions;
            }

            set
            {
                if (value != this._transactions)
                {
                    this._transactions = value;
                    this.OnPropertyChanged("Transactions");
                }
            }
        }
    }
}
