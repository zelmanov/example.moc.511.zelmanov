﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AdventureWorks.TransactionHistory.Views
{
    public class PlotPoint : Control
    {
        public static readonly DependencyProperty TransactionDateProperty = DependencyProperty.Register(
            "TransactionDate",
            typeof(DateTime),
            typeof(PlotPoint),
            new UIPropertyMetadata(DateTime.MinValue));
        public static readonly DependencyProperty XProperty = DependencyProperty.Register(
            "X", 
            typeof(double), 
            typeof(PlotPoint), 
            new UIPropertyMetadata(0.0));
        public static readonly DependencyProperty YProperty = DependencyProperty.Register(
            "Y",
            typeof(double),
            typeof(PlotPoint),
            new UIPropertyMetadata(0.0));

        public static readonly RoutedEvent ClickEvent = EventManager.RegisterRoutedEvent(
            "Click",
            RoutingStrategy.Bubble,
            typeof(RoutedEventHandler),
            typeof(PlotPoint));

        static PlotPoint()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(PlotPoint), 
                new FrameworkPropertyMetadata(typeof(PlotPoint)));
        }

        public PlotPoint() : this(DateTime.Now, 0.0, 0.0)
        {
        }

        public PlotPoint(DateTime date, double xPosition, double yPosition)
        {
            this.DataContext = this;
            this.TransactionDate = date;
            this.X = xPosition;
            this.Y = yPosition;
        }

        public event RoutedEventHandler Click
        {
            add { this.AddHandler(ClickEvent, value); }
            remove { this.RemoveHandler(ClickEvent, value); }
        }

        public DateTime TransactionDate
        {
            get { return (DateTime)GetValue(TransactionDateProperty); }
            set { SetValue(TransactionDateProperty, value); }
        }

        public double X
        {
            get { return (double)GetValue(XProperty); }
            set { SetValue(XProperty, value); }
        }

        public double Y
        {
            get { return (double)GetValue(YProperty); }
            set { SetValue(YProperty, value); }
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            this.RaiseClickEvent();
        }

        private void RaiseClickEvent()
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(PlotPoint.ClickEvent);
            this.RaiseEvent(eventArgs);
        }
    }
}
