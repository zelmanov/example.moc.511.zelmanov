﻿using System;
using System.Windows.Forms;
using AdventureWorks.Models;
using AdventureWorks.ProductInventory.Presenters;

namespace AdventureWorks.ProductInventory.Views
{
    public partial class MainView : ViewBase
    {
        private MainViewPresenter _presenter;

        public MainView()
            : this(null)
        {
        }
        
        public MainView(MainViewPresenter presenter)
        {
            this._presenter = base.SetPresenter(presenter ?? new MainViewPresenter());
            InitializeComponent();
        }

        protected override void InitializeDataBinding()
        {
            // Set the DataSource for the Categories List
            this.comboBoxCategory.DisplayMember = this._presenter.CategoryDisplayMember;
            this.comboBoxCategory.DataSource = this._presenter.CategoryDataSource;

            // Set the DataSource and binding for the Subcategories List
            this.comboBoxSubcategory.DisplayMember = this._presenter.CategoryDisplayMember;
            this.comboBoxSubcategory.DataBindings.Add(
                new Binding("DataSource", this._presenter, "SubcategoryDataSource"));

            // Set the binding for the Products list
            this.dataGridViewProducts.DataBindings.Add(
                new Binding("DataSource", this._presenter, "ProductsDataSource"));

            // Set the binding for the search box
            this.textBoxSearch.DataBindings.Add(
                new Binding("Text", this._presenter, "SearchCriteria"));
        }

        private void comboBoxCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._presenter.SetCategory(
                (ProductCategory)this.comboBoxCategory.SelectedItem);
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            using (new WaitCursor(this))
            {
                this._presenter.Search();
            }
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            using (new WaitCursor(this))
            {
                this._presenter.SetSubcategory(
                    (ProductSubcategory)this.comboBoxSubcategory.SelectedItem);
            }
        }

        private void dataGridViewProducts_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            using (new WaitCursor(this))
            {
                this._presenter.SetProduct(e.RowIndex);    
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}