﻿using System.Windows;
using AdventureWorks.Model;
using AdventureWorks.WorkOrders.Views;

namespace AdventureWorks.WorkOrders
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Called when the application starts.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // The View.
            var window = new MainWindowView();

            // The data service.
            var dataService = new DataAccessService();

            // The ViewModel.
            window.DataContext = new MainWindowViewModel(window, dataService);
            
            // Show the main window.
            window.Show();
        }
    }
}
