﻿namespace AdventureWorks.ProductInventory.Views
{
    partial class ProductView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label productNumberLabel;
            System.Windows.Forms.Label listPriceLabel;
            System.Windows.Forms.Label daysToManufactureLabel;
            System.Windows.Forms.Label sellEndDateLabel;
            System.Windows.Forms.Label productLineLabel;
            System.Windows.Forms.Label sellStartDateLabel;
            System.Windows.Forms.Label standardCostLabel;
            System.Windows.Forms.Label locationNameLabel;
            System.Windows.Forms.Label reorderPointLabel;
            System.Windows.Forms.Label safetyStockLevelLabel;
            System.Windows.Forms.Label classLabel;
            System.Windows.Forms.Label colorLabel;
            System.Windows.Forms.Label sizeLabel;
            System.Windows.Forms.Label styleLabel;
            System.Windows.Forms.Label weightLabel;
            this.buttonClose = new System.Windows.Forms.Button();
            this.productBindingSource = new System.Windows.Forms.BindingSource();
            this.productInventoriesBindingSource = new System.Windows.Forms.BindingSource();
            this.productPhotosBindingSource = new System.Windows.Forms.BindingSource();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabDetails = new System.Windows.Forms.TabPage();
            this.groupBoxDetails = new System.Windows.Forms.GroupBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.productNumberTextBox = new System.Windows.Forms.TextBox();
            this.listPriceTextBox = new System.Windows.Forms.TextBox();
            this.daysToManufactureTextBox = new System.Windows.Forms.TextBox();
            this.sellEndDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.productLineTextBox = new System.Windows.Forms.TextBox();
            this.sellStartDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.standardCostTextBox = new System.Windows.Forms.TextBox();
            this.groupBoxInventory = new System.Windows.Forms.GroupBox();
            this.nameTextBox1 = new System.Windows.Forms.TextBox();
            this.productInventoriesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reorderPointTextBox = new System.Windows.Forms.TextBox();
            this.safetyStockLevelTextBox = new System.Windows.Forms.TextBox();
            this.groupBoxSpecification = new System.Windows.Forms.GroupBox();
            this.classTextBox = new System.Windows.Forms.TextBox();
            this.colorTextBox = new System.Windows.Forms.TextBox();
            this.sizeUnitMeasureCodeTextBox = new System.Windows.Forms.TextBox();
            this.sizeTextBox = new System.Windows.Forms.TextBox();
            this.weightUnitMeasureCodeTextBox = new System.Windows.Forms.TextBox();
            this.weightTextBox = new System.Windows.Forms.TextBox();
            this.styleTextBox = new System.Windows.Forms.TextBox();
            this.groupBoxPicture = new System.Windows.Forms.GroupBox();
            this.largePhotoPictureBox = new System.Windows.Forms.PictureBox();
            this.tabHistory = new System.Windows.Forms.TabPage();
            nameLabel = new System.Windows.Forms.Label();
            productNumberLabel = new System.Windows.Forms.Label();
            listPriceLabel = new System.Windows.Forms.Label();
            daysToManufactureLabel = new System.Windows.Forms.Label();
            sellEndDateLabel = new System.Windows.Forms.Label();
            productLineLabel = new System.Windows.Forms.Label();
            sellStartDateLabel = new System.Windows.Forms.Label();
            standardCostLabel = new System.Windows.Forms.Label();
            locationNameLabel = new System.Windows.Forms.Label();
            reorderPointLabel = new System.Windows.Forms.Label();
            safetyStockLevelLabel = new System.Windows.Forms.Label();
            classLabel = new System.Windows.Forms.Label();
            colorLabel = new System.Windows.Forms.Label();
            sizeLabel = new System.Windows.Forms.Label();
            styleLabel = new System.Windows.Forms.Label();
            weightLabel = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabDetails.SuspendLayout();
            this.groupBoxDetails.SuspendLayout();
            this.groupBoxInventory.SuspendLayout();
            this.groupBoxSpecification.SuspendLayout();
            this.groupBoxPicture.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.Location = new System.Drawing.Point(734, 547);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 57;
            this.buttonClose.Text = "&Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataSource = typeof(AdventureWorks.Models.Product);
            // 
            // productInventoriesBindingSource
            // 
            this.productInventoriesBindingSource.DataMember = "ProductInventories";
            this.productInventoriesBindingSource.DataSource = this.productBindingSource;
            // 
            // productPhotosBindingSource
            // 
            this.productPhotosBindingSource.DataMember = "ProductPhotos";
            this.productPhotosBindingSource.DataSource = this.productBindingSource;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabDetails);
            this.tabControl.Controls.Add(this.tabHistory);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(790, 529);
            this.tabControl.TabIndex = 60;
            // 
            // tabDetails
            // 
            this.tabDetails.Controls.Add(this.groupBoxDetails);
            this.tabDetails.Controls.Add(this.groupBoxInventory);
            this.tabDetails.Controls.Add(this.groupBoxSpecification);
            this.tabDetails.Controls.Add(this.groupBoxPicture);
            this.tabDetails.Location = new System.Drawing.Point(4, 22);
            this.tabDetails.Name = "tabDetails";
            this.tabDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetails.Size = new System.Drawing.Size(782, 503);
            this.tabDetails.TabIndex = 0;
            this.tabDetails.Text = "Product Details";
            this.tabDetails.UseVisualStyleBackColor = true;
            // 
            // groupBoxDetails
            // 
            this.groupBoxDetails.Controls.Add(nameLabel);
            this.groupBoxDetails.Controls.Add(this.nameTextBox);
            this.groupBoxDetails.Controls.Add(productNumberLabel);
            this.groupBoxDetails.Controls.Add(this.productNumberTextBox);
            this.groupBoxDetails.Controls.Add(listPriceLabel);
            this.groupBoxDetails.Controls.Add(daysToManufactureLabel);
            this.groupBoxDetails.Controls.Add(this.listPriceTextBox);
            this.groupBoxDetails.Controls.Add(this.daysToManufactureTextBox);
            this.groupBoxDetails.Controls.Add(sellEndDateLabel);
            this.groupBoxDetails.Controls.Add(productLineLabel);
            this.groupBoxDetails.Controls.Add(this.sellEndDateDateTimePicker);
            this.groupBoxDetails.Controls.Add(this.productLineTextBox);
            this.groupBoxDetails.Controls.Add(sellStartDateLabel);
            this.groupBoxDetails.Controls.Add(this.sellStartDateDateTimePicker);
            this.groupBoxDetails.Controls.Add(this.standardCostTextBox);
            this.groupBoxDetails.Controls.Add(standardCostLabel);
            this.groupBoxDetails.Location = new System.Drawing.Point(5, 6);
            this.groupBoxDetails.Name = "groupBoxDetails";
            this.groupBoxDetails.Size = new System.Drawing.Size(385, 325);
            this.groupBoxDetails.TabIndex = 60;
            this.groupBoxDetails.TabStop = false;
            this.groupBoxDetails.Text = "Product Details";
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(37, 46);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(38, 13);
            nameLabel.TabIndex = 18;
            nameLabel.Text = "Name:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(156, 46);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(200, 20);
            this.nameTextBox.TabIndex = 1;
            // 
            // productNumberLabel
            // 
            productNumberLabel.AutoSize = true;
            productNumberLabel.Location = new System.Drawing.Point(37, 74);
            productNumberLabel.Name = "productNumberLabel";
            productNumberLabel.Size = new System.Drawing.Size(87, 13);
            productNumberLabel.TabIndex = 26;
            productNumberLabel.Text = "Product Number:";
            // 
            // productNumberTextBox
            // 
            this.productNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ProductNumber", true));
            this.productNumberTextBox.Location = new System.Drawing.Point(156, 73);
            this.productNumberTextBox.Name = "productNumberTextBox";
            this.productNumberTextBox.Size = new System.Drawing.Size(200, 20);
            this.productNumberTextBox.TabIndex = 2;
            // 
            // listPriceLabel
            // 
            listPriceLabel.AutoSize = true;
            listPriceLabel.Location = new System.Drawing.Point(37, 221);
            listPriceLabel.Name = "listPriceLabel";
            listPriceLabel.Size = new System.Drawing.Size(53, 13);
            listPriceLabel.TabIndex = 12;
            listPriceLabel.Text = "List Price:";
            // 
            // daysToManufactureLabel
            // 
            daysToManufactureLabel.AutoSize = true;
            daysToManufactureLabel.Location = new System.Drawing.Point(37, 165);
            daysToManufactureLabel.Name = "daysToManufactureLabel";
            daysToManufactureLabel.Size = new System.Drawing.Size(113, 13);
            daysToManufactureLabel.TabIndex = 6;
            daysToManufactureLabel.Text = "Days To Manufacture:";
            // 
            // listPriceTextBox
            // 
            this.listPriceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ListPrice", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "C2"));
            this.listPriceTextBox.Location = new System.Drawing.Point(156, 217);
            this.listPriceTextBox.Name = "listPriceTextBox";
            this.listPriceTextBox.Size = new System.Drawing.Size(200, 20);
            this.listPriceTextBox.TabIndex = 6;
            // 
            // daysToManufactureTextBox
            // 
            this.daysToManufactureTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "DaysToManufacture", true));
            this.daysToManufactureTextBox.Location = new System.Drawing.Point(156, 162);
            this.daysToManufactureTextBox.Name = "daysToManufactureTextBox";
            this.daysToManufactureTextBox.Size = new System.Drawing.Size(200, 20);
            this.daysToManufactureTextBox.TabIndex = 4;
            // 
            // sellEndDateLabel
            // 
            sellEndDateLabel.AutoSize = true;
            sellEndDateLabel.Location = new System.Drawing.Point(37, 247);
            sellEndDateLabel.Name = "sellEndDateLabel";
            sellEndDateLabel.Size = new System.Drawing.Size(75, 13);
            sellEndDateLabel.TabIndex = 36;
            sellEndDateLabel.Text = "Sell End Date:";
            // 
            // productLineLabel
            // 
            productLineLabel.AutoSize = true;
            productLineLabel.Location = new System.Drawing.Point(37, 102);
            productLineLabel.Name = "productLineLabel";
            productLineLabel.Size = new System.Drawing.Size(70, 13);
            productLineLabel.TabIndex = 22;
            productLineLabel.Text = "Product Line:";
            // 
            // sellEndDateDateTimePicker
            // 
            this.sellEndDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.productBindingSource, "SellEndDate", true));
            this.sellEndDateDateTimePicker.Location = new System.Drawing.Point(156, 240);
            this.sellEndDateDateTimePicker.Name = "sellEndDateDateTimePicker";
            this.sellEndDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.sellEndDateDateTimePicker.TabIndex = 7;
            // 
            // productLineTextBox
            // 
            this.productLineTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ProductLine", true));
            this.productLineTextBox.Location = new System.Drawing.Point(156, 100);
            this.productLineTextBox.Name = "productLineTextBox";
            this.productLineTextBox.Size = new System.Drawing.Size(200, 20);
            this.productLineTextBox.TabIndex = 3;
            // 
            // sellStartDateLabel
            // 
            sellStartDateLabel.AutoSize = true;
            sellStartDateLabel.Location = new System.Drawing.Point(37, 273);
            sellStartDateLabel.Name = "sellStartDateLabel";
            sellStartDateLabel.Size = new System.Drawing.Size(78, 13);
            sellStartDateLabel.TabIndex = 38;
            sellStartDateLabel.Text = "Sell Start Date:";
            // 
            // sellStartDateDateTimePicker
            // 
            this.sellStartDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.productBindingSource, "SellStartDate", true));
            this.sellStartDateDateTimePicker.Location = new System.Drawing.Point(156, 270);
            this.sellStartDateDateTimePicker.Name = "sellStartDateDateTimePicker";
            this.sellStartDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.sellStartDateDateTimePicker.TabIndex = 8;
            // 
            // standardCostTextBox
            // 
            this.standardCostTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "StandardCost", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "C2"));
            this.standardCostTextBox.Location = new System.Drawing.Point(156, 190);
            this.standardCostTextBox.Name = "standardCostTextBox";
            this.standardCostTextBox.Size = new System.Drawing.Size(200, 20);
            this.standardCostTextBox.TabIndex = 5;
            // 
            // standardCostLabel
            // 
            standardCostLabel.AutoSize = true;
            standardCostLabel.Location = new System.Drawing.Point(37, 193);
            standardCostLabel.Name = "standardCostLabel";
            standardCostLabel.Size = new System.Drawing.Size(77, 13);
            standardCostLabel.TabIndex = 44;
            standardCostLabel.Text = "Standard Cost:";
            // 
            // groupBoxInventory
            // 
            this.groupBoxInventory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxInventory.Controls.Add(locationNameLabel);
            this.groupBoxInventory.Controls.Add(this.nameTextBox1);
            this.groupBoxInventory.Controls.Add(this.productInventoriesDataGridView);
            this.groupBoxInventory.Controls.Add(reorderPointLabel);
            this.groupBoxInventory.Controls.Add(this.reorderPointTextBox);
            this.groupBoxInventory.Controls.Add(safetyStockLevelLabel);
            this.groupBoxInventory.Controls.Add(this.safetyStockLevelTextBox);
            this.groupBoxInventory.Location = new System.Drawing.Point(5, 337);
            this.groupBoxInventory.Name = "groupBoxInventory";
            this.groupBoxInventory.Size = new System.Drawing.Size(771, 160);
            this.groupBoxInventory.TabIndex = 59;
            this.groupBoxInventory.TabStop = false;
            this.groupBoxInventory.Text = "Product Inventory";
            // 
            // locationNameLabel
            // 
            locationNameLabel.AutoSize = true;
            locationNameLabel.Location = new System.Drawing.Point(37, 51);
            locationNameLabel.Name = "locationNameLabel";
            locationNameLabel.Size = new System.Drawing.Size(96, 13);
            locationNameLabel.TabIndex = 57;
            locationNameLabel.Text = "Selected Location:";
            // 
            // nameTextBox1
            // 
            this.nameTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productInventoriesBindingSource, "Location.Name", true));
            this.nameTextBox1.Location = new System.Drawing.Point(156, 48);
            this.nameTextBox1.Name = "nameTextBox1";
            this.nameTextBox1.Size = new System.Drawing.Size(200, 20);
            this.nameTextBox1.TabIndex = 58;
            // 
            // productInventoriesDataGridView
            // 
            this.productInventoriesDataGridView.AllowUserToAddRows = false;
            this.productInventoriesDataGridView.AllowUserToDeleteRows = false;
            this.productInventoriesDataGridView.AllowUserToOrderColumns = true;
            this.productInventoriesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.productInventoriesDataGridView.AutoGenerateColumns = false;
            this.productInventoriesDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.productInventoriesDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.productInventoriesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productInventoriesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.productInventoriesDataGridView.DataSource = this.productInventoriesBindingSource;
            this.productInventoriesDataGridView.Location = new System.Drawing.Point(40, 74);
            this.productInventoriesDataGridView.Name = "productInventoriesDataGridView";
            this.productInventoriesDataGridView.ReadOnly = true;
            this.productInventoriesDataGridView.Size = new System.Drawing.Size(705, 80);
            this.productInventoriesDataGridView.TabIndex = 18;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "LocationID";
            this.dataGridViewTextBoxColumn2.HeaderText = "LocationID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Shelf";
            this.dataGridViewTextBoxColumn3.HeaderText = "Shelf";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Bin";
            this.dataGridViewTextBoxColumn4.HeaderText = "Bin";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Quantity";
            this.dataGridViewTextBoxColumn5.HeaderText = "Quantity";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // reorderPointLabel
            // 
            reorderPointLabel.AutoSize = true;
            reorderPointLabel.Location = new System.Drawing.Point(37, 22);
            reorderPointLabel.Name = "reorderPointLabel";
            reorderPointLabel.Size = new System.Drawing.Size(75, 13);
            reorderPointLabel.TabIndex = 30;
            reorderPointLabel.Text = "Reorder Point:";
            // 
            // reorderPointTextBox
            // 
            this.reorderPointTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "ReorderPoint", true));
            this.reorderPointTextBox.Location = new System.Drawing.Point(156, 19);
            this.reorderPointTextBox.Name = "reorderPointTextBox";
            this.reorderPointTextBox.Size = new System.Drawing.Size(200, 20);
            this.reorderPointTextBox.TabIndex = 16;
            // 
            // safetyStockLevelLabel
            // 
            safetyStockLevelLabel.AutoSize = true;
            safetyStockLevelLabel.Location = new System.Drawing.Point(423, 22);
            safetyStockLevelLabel.Name = "safetyStockLevelLabel";
            safetyStockLevelLabel.Size = new System.Drawing.Size(100, 13);
            safetyStockLevelLabel.TabIndex = 34;
            safetyStockLevelLabel.Text = "Safety Stock Level:";
            // 
            // safetyStockLevelTextBox
            // 
            this.safetyStockLevelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "SafetyStockLevel", true));
            this.safetyStockLevelTextBox.Location = new System.Drawing.Point(542, 19);
            this.safetyStockLevelTextBox.Name = "safetyStockLevelTextBox";
            this.safetyStockLevelTextBox.Size = new System.Drawing.Size(203, 20);
            this.safetyStockLevelTextBox.TabIndex = 17;
            // 
            // groupBoxSpecification
            // 
            this.groupBoxSpecification.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSpecification.Controls.Add(classLabel);
            this.groupBoxSpecification.Controls.Add(this.classTextBox);
            this.groupBoxSpecification.Controls.Add(colorLabel);
            this.groupBoxSpecification.Controls.Add(this.colorTextBox);
            this.groupBoxSpecification.Controls.Add(sizeLabel);
            this.groupBoxSpecification.Controls.Add(this.sizeUnitMeasureCodeTextBox);
            this.groupBoxSpecification.Controls.Add(this.sizeTextBox);
            this.groupBoxSpecification.Controls.Add(styleLabel);
            this.groupBoxSpecification.Controls.Add(this.weightUnitMeasureCodeTextBox);
            this.groupBoxSpecification.Controls.Add(this.weightTextBox);
            this.groupBoxSpecification.Controls.Add(weightLabel);
            this.groupBoxSpecification.Controls.Add(this.styleTextBox);
            this.groupBoxSpecification.Location = new System.Drawing.Point(396, 162);
            this.groupBoxSpecification.Name = "groupBoxSpecification";
            this.groupBoxSpecification.Size = new System.Drawing.Size(406, 169);
            this.groupBoxSpecification.TabIndex = 58;
            this.groupBoxSpecification.TabStop = false;
            this.groupBoxSpecification.Text = "Product Specification";
            // 
            // classLabel
            // 
            classLabel.AutoSize = true;
            classLabel.Location = new System.Drawing.Point(32, 37);
            classLabel.Name = "classLabel";
            classLabel.Size = new System.Drawing.Size(35, 13);
            classLabel.TabIndex = 2;
            classLabel.Text = "Class:";
            // 
            // classTextBox
            // 
            this.classTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.classTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "Class", true));
            this.classTextBox.Location = new System.Drawing.Point(151, 34);
            this.classTextBox.Name = "classTextBox";
            this.classTextBox.Size = new System.Drawing.Size(203, 20);
            this.classTextBox.TabIndex = 9;
            // 
            // colorLabel
            // 
            colorLabel.AutoSize = true;
            colorLabel.Location = new System.Drawing.Point(32, 63);
            colorLabel.Name = "colorLabel";
            colorLabel.Size = new System.Drawing.Size(34, 13);
            colorLabel.TabIndex = 4;
            colorLabel.Text = "Color:";
            // 
            // colorTextBox
            // 
            this.colorTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.colorTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "Color", true));
            this.colorTextBox.Location = new System.Drawing.Point(151, 60);
            this.colorTextBox.Name = "colorTextBox";
            this.colorTextBox.Size = new System.Drawing.Size(203, 20);
            this.colorTextBox.TabIndex = 10;
            // 
            // sizeLabel
            // 
            sizeLabel.AutoSize = true;
            sizeLabel.Location = new System.Drawing.Point(32, 89);
            sizeLabel.Name = "sizeLabel";
            sizeLabel.Size = new System.Drawing.Size(30, 13);
            sizeLabel.TabIndex = 40;
            sizeLabel.Text = "Size:";
            // 
            // sizeUnitMeasureCodeTextBox
            // 
            this.sizeUnitMeasureCodeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.sizeUnitMeasureCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "SizeUnitMeasureCode", true));
            this.sizeUnitMeasureCodeTextBox.Location = new System.Drawing.Point(304, 86);
            this.sizeUnitMeasureCodeTextBox.Name = "sizeUnitMeasureCodeTextBox";
            this.sizeUnitMeasureCodeTextBox.Size = new System.Drawing.Size(50, 20);
            this.sizeUnitMeasureCodeTextBox.TabIndex = 12;
            // 
            // sizeTextBox
            // 
            this.sizeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "Size", true));
            this.sizeTextBox.Location = new System.Drawing.Point(151, 86);
            this.sizeTextBox.Name = "sizeTextBox";
            this.sizeTextBox.Size = new System.Drawing.Size(147, 20);
            this.sizeTextBox.TabIndex = 11;
            // 
            // styleLabel
            // 
            styleLabel.AutoSize = true;
            styleLabel.Location = new System.Drawing.Point(32, 115);
            styleLabel.Name = "styleLabel";
            styleLabel.Size = new System.Drawing.Size(33, 13);
            styleLabel.TabIndex = 46;
            styleLabel.Text = "Style:";
            // 
            // weightUnitMeasureCodeTextBox
            // 
            this.weightUnitMeasureCodeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.weightUnitMeasureCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "WeightUnitMeasureCode", true));
            this.weightUnitMeasureCodeTextBox.Location = new System.Drawing.Point(304, 138);
            this.weightUnitMeasureCodeTextBox.Name = "weightUnitMeasureCodeTextBox";
            this.weightUnitMeasureCodeTextBox.Size = new System.Drawing.Size(50, 20);
            this.weightUnitMeasureCodeTextBox.TabIndex = 15;
            // 
            // weightTextBox
            // 
            this.weightTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "Weight", true));
            this.weightTextBox.Location = new System.Drawing.Point(151, 138);
            this.weightTextBox.Name = "weightTextBox";
            this.weightTextBox.Size = new System.Drawing.Size(147, 20);
            this.weightTextBox.TabIndex = 14;
            // 
            // weightLabel
            // 
            weightLabel.AutoSize = true;
            weightLabel.Location = new System.Drawing.Point(32, 141);
            weightLabel.Name = "weightLabel";
            weightLabel.Size = new System.Drawing.Size(44, 13);
            weightLabel.TabIndex = 48;
            weightLabel.Text = "Weight:";
            // 
            // styleTextBox
            // 
            this.styleTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.styleTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "Style", true));
            this.styleTextBox.Location = new System.Drawing.Point(151, 112);
            this.styleTextBox.Name = "styleTextBox";
            this.styleTextBox.Size = new System.Drawing.Size(203, 20);
            this.styleTextBox.TabIndex = 13;
            // 
            // groupBoxPicture
            // 
            this.groupBoxPicture.Controls.Add(this.largePhotoPictureBox);
            this.groupBoxPicture.Location = new System.Drawing.Point(396, 6);
            this.groupBoxPicture.Name = "groupBoxPicture";
            this.groupBoxPicture.Size = new System.Drawing.Size(200, 150);
            this.groupBoxPicture.TabIndex = 57;
            this.groupBoxPicture.TabStop = false;
            this.groupBoxPicture.Text = "Product Picture";
            // 
            // largePhotoPictureBox
            // 
            this.largePhotoPictureBox.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.productPhotosBindingSource, "ProductPhoto.LargePhoto", true));
            this.largePhotoPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.largePhotoPictureBox.Location = new System.Drawing.Point(3, 16);
            this.largePhotoPictureBox.Name = "largePhotoPictureBox";
            this.largePhotoPictureBox.Size = new System.Drawing.Size(194, 131);
            this.largePhotoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.largePhotoPictureBox.TabIndex = 52;
            this.largePhotoPictureBox.TabStop = false;
            // 
            // tabHistory
            // 
            this.tabHistory.Location = new System.Drawing.Point(4, 22);
            this.tabHistory.Name = "tabHistory";
            this.tabHistory.Padding = new System.Windows.Forms.Padding(3);
            this.tabHistory.Size = new System.Drawing.Size(782, 503);
            this.tabHistory.TabIndex = 1;
            this.tabHistory.Text = "Transaction History";
            this.tabHistory.UseVisualStyleBackColor = true;
            // 
            // ProductView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 582);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.buttonClose);
            this.Name = "ProductView";
            this.Text = "Adventure Works Cycles - Product: {0}";
            this.tabControl.ResumeLayout(false);
            this.tabDetails.ResumeLayout(false);
            this.groupBoxDetails.ResumeLayout(false);
            this.groupBoxDetails.PerformLayout();
            this.groupBoxInventory.ResumeLayout(false);
            this.groupBoxInventory.PerformLayout();
            this.groupBoxSpecification.ResumeLayout(false);
            this.groupBoxSpecification.PerformLayout();
            this.groupBoxPicture.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.BindingSource productBindingSource;
        private System.Windows.Forms.BindingSource productInventoriesBindingSource;
        private System.Windows.Forms.BindingSource productPhotosBindingSource;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabDetails;
        private System.Windows.Forms.GroupBox groupBoxDetails;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox productNumberTextBox;
        private System.Windows.Forms.TextBox listPriceTextBox;
        private System.Windows.Forms.TextBox daysToManufactureTextBox;
        private System.Windows.Forms.DateTimePicker sellEndDateDateTimePicker;
        private System.Windows.Forms.TextBox productLineTextBox;
        private System.Windows.Forms.DateTimePicker sellStartDateDateTimePicker;
        private System.Windows.Forms.TextBox standardCostTextBox;
        private System.Windows.Forms.GroupBox groupBoxInventory;
        private System.Windows.Forms.TextBox nameTextBox1;
        private System.Windows.Forms.DataGridView productInventoriesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.TextBox reorderPointTextBox;
        private System.Windows.Forms.TextBox safetyStockLevelTextBox;
        private System.Windows.Forms.GroupBox groupBoxSpecification;
        private System.Windows.Forms.TextBox classTextBox;
        private System.Windows.Forms.TextBox colorTextBox;
        private System.Windows.Forms.TextBox sizeUnitMeasureCodeTextBox;
        private System.Windows.Forms.TextBox sizeTextBox;
        private System.Windows.Forms.TextBox weightUnitMeasureCodeTextBox;
        private System.Windows.Forms.TextBox weightTextBox;
        private System.Windows.Forms.TextBox styleTextBox;
        private System.Windows.Forms.GroupBox groupBoxPicture;
        private System.Windows.Forms.PictureBox largePhotoPictureBox;
        private System.Windows.Forms.TabPage tabHistory;
    }
}