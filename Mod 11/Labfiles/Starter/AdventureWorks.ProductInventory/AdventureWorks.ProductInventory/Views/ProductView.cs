﻿using System;
using System.Windows.Forms;
using AdventureWorks.ProductInventory.Presenters;

namespace AdventureWorks.ProductInventory.Views
{
    public partial class ProductView : ViewBase
    {
        private ProductViewPresenter _presenter;

        public ProductView()
            : this(null)
        {
        }

        public ProductView(ProductViewPresenter presenter)
        {
            this._presenter = base.SetPresenter(presenter ?? new ProductViewPresenter());
            InitializeComponent();
        }

        protected override void InitializeDataBinding()
        {
            this.Text = string.Format(this.Text, this._presenter.Model.Name);
            this.productBindingSource.DataSource = this._presenter.Model;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}