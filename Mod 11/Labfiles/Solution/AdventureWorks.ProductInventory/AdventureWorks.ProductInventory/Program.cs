﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using AdventureWorks.ProductInventory.Presenters;
using AdventureWorks.ProductInventory.Views;

namespace AdventureWorks.ProductInventory
{
    /// <summary>
    /// Contains the application entry point
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var presenter = new MainViewPresenter();
            presenter.PropertyChanged += PresenterPropertyChanged;
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainView(presenter));
        }

        /// <summary>
        /// Process presenter messages.
        /// </summary>
        /// <param name="sender">Source presenter.</param>
        /// <param name="e">Arguments for the message.</param>
        private static void PresenterPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "SelectedProduct":
                    var prodPresenter = new ProductViewPresenter(((MainViewPresenter)sender).SelectedProduct);
                    var prodView = new ProductView(prodPresenter);
                    prodView.ShowDialog();
                    break;
                default:
                    break;
            }            
        }
    }
}