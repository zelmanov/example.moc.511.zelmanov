﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AdventureWorks.Models;

namespace AdventureWorks.TransactionHistory.Views
{
    /// <summary>
    /// Interaction logic for TransactionHistoryView.xaml
    /// </summary>
    public partial class TransactionHistoryView : UserControl
    {
        public static readonly DependencyProperty BaseColorProperty = DependencyProperty.Register(
            "BaseColor",
            typeof(Color),
            typeof(TransactionHistoryView),
            new UIPropertyMetadata(Color.FromRgb(0, 188, 255), BaseColor_Changed));

        public static readonly DependencyProperty GraphBackgroundProperty = DependencyProperty.Register(
            "GraphBackground",
            typeof(BitmapImage),
            typeof(TransactionHistoryView),
            new UIPropertyMetadata(null, GraphBackground_Changed));

        public static readonly DependencyProperty ProductProperty = DependencyProperty.Register(
            "Product",
            typeof(Product),
            typeof(TransactionHistoryView),
            new UIPropertyMetadata(null));

        private DropShadowEffect dropShadow = new DropShadowEffect();

        public TransactionHistoryView()
        {
            this.ViewModel = new TransactionHistoryViewModel(this);
            InitializeComponent();
        }

        private static void BaseColor_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TransactionHistoryView source = (TransactionHistoryView)d;
            source.Resources["BaseColor"] = (Color)e.NewValue;
        }

        private static void GraphBackground_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TransactionHistoryView source = (TransactionHistoryView)d;
            source.UpdateBackground((BitmapImage)e.NewValue);
        }

        public Color BaseColor
        {
            get { return (Color)GetValue(BaseColorProperty); }
            set { SetValue(BaseColorProperty, value); }
        }

        public BitmapImage GraphBackground
        {
            get { return (BitmapImage)GetValue(GraphBackgroundProperty); }
            set { SetValue(GraphBackgroundProperty, value); }
        }

        public Product Product
        {
            get { return (Product)GetValue(ProductProperty); }
            set { SetValue(ProductProperty, value); }
        }

        public double GraphHeight
        {
            get { return this._graph.ActualHeight; }
        }

        public double GraphWidth
        {
            get { return this._graph.ActualWidth; }
        }

        public TransactionHistoryViewModel ViewModel
        {
            get { return this.DataContext as TransactionHistoryViewModel; }
            private set { this.DataContext = value; }
        }

        public void AddPlotPoint(double x, double y)
        {
            Ellipse plotPoint = new Ellipse();
            plotPoint.Height = 6;
            plotPoint.Width = 6;
            plotPoint.Fill = (Brush)this.Resources["BaseColorBrush"];
            plotPoint.Effect = this.dropShadow;
            plotPoint.SetValue(Canvas.LeftProperty, x - 3);
            plotPoint.SetValue(Canvas.TopProperty, y - 3);
            this._graph.Children.Add(plotPoint);
        }

        public void ClearPlotPoints()
        {
            this._graph.Children.Clear();
        }

        private void UpdateBackground(BitmapImage image)
        {
            ImageBrush brush = (ImageBrush)this._graph.Background;
            brush.ImageSource = image;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.Product = this.Product;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.Dispose();
        }
    }
}
