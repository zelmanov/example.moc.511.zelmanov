using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;

namespace AdventureWorks.TransactionHistory.Views
{
    public abstract class ViewModelBase : IDisposable, INotifyPropertyChanged
    {
        private static bool? _isInDesignMode;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        protected ViewModelBase()
        {
        }

        ~ViewModelBase()
        {
            this.Dispose(false);
            if (false == IsInDesignModeStatic)
            {
                Debug.Fail("Dispose not called on ViewModel class.");
            }
        }

        public static bool IsInDesignModeStatic
        {
            get
            {
                if (false == _isInDesignMode.HasValue)
                {
                    var prop = DesignerProperties.IsInDesignModeProperty;
                    _isInDesignMode = (bool)DependencyPropertyDescriptor
                        .FromProperty(prop, typeof(FrameworkElement))
                        .Metadata.DefaultValue;

                    if (false == _isInDesignMode.Value)
                    {
                        if (true == System.Diagnostics.Process
                            .GetCurrentProcess()
                            .ProcessName
                            .StartsWith("devenv"))
                        {
                            _isInDesignMode = true;
                        }
                    }
                }

                return _isInDesignMode.Value;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.EnsureProperty(propertyName);
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        [Conditional("DEBUG")]
        private void EnsureProperty(string propertyName)
        {
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                throw new ArgumentException("Property does not exist.", "propertyName");
            }
        }
    }
}
