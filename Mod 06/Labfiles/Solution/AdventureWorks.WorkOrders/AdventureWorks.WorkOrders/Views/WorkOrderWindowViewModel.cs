﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdventureWorks.Model;
using System.ComponentModel;

namespace AdventureWorks.WorkOrders.Views
{
    public class WorkOrderWindowViewModel : INotifyPropertyChanged
    {
        private WorkOrder _order;

        public WorkOrder Order
        {
            get { return this._order; }
            set
            {
                if (this._order != value)
                {
                    this._order = value;
                    this.OnPropertyChanged("Order");
                }
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
