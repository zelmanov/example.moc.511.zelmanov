﻿using System;
using System.Collections.ObjectModel;
using AdventureWorks.Model;

namespace AdventureWorks.WorkOrders.Views
{
    public class ScratchPadViewModel : ViewModelBase
    {
        public ScratchPadViewModel(ScratchPadView scratchPad) : base()
        {
            this.View = scratchPad;
            this.WorkOrders = new ObservableCollection<WorkOrder>();

            // TODO: Ex1. Task4. Implement Drag-and-Drop by Using an Attached Behavior. Dropped Event.
        }

        public ScratchPadView View { get; private set; }

        public ObservableCollection<WorkOrder> WorkOrders { get; private set; }

        // TODO: Ex1. Task4. Implement Drag-and-Drop by Using an Attached Behavior. OnDropped Handler.
    }
}
