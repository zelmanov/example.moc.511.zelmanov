﻿using System.Windows;
using System.Windows.Controls;
using AdventureWorks.Interactivity;

namespace AdventureWorks.WorkOrders.Views
{
    /// <summary>
    /// Interaction logic for ScratchPadView.xaml
    /// </summary>
    public partial class ScratchPadView : UserControl
    {
        public ScratchPadView()
        {
            this.ViewModel = new ScratchPadViewModel(this);
            InitializeComponent();
        }

        public ScratchPadViewModel ViewModel
        {
            get { return this.DataContext as ScratchPadViewModel; }
            set { this.DataContext = value; }
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.Dispose();
        }
		
        private void OnDropped(object sender, DragDropEventArgs e)
        {
            this.ViewModel.OnDropped(sender, e);
        }
    }
}
