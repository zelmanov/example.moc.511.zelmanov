﻿using System;
using System.Collections.ObjectModel;
using AdventureWorks.Interactivity;
using AdventureWorks.Model;

namespace AdventureWorks.WorkOrders.Views
{
    public class ScratchPadViewModel : ViewModelBase
    {
        public ScratchPadViewModel(ScratchPadView scratchPad) : base()
        {
            this.View = scratchPad;
            this.WorkOrders = new ObservableCollection<WorkOrder>();
        }

        public ScratchPadView View { get; private set; }

        public ObservableCollection<WorkOrder> WorkOrders { get; private set; }

        public void OnDropped(object sender, DragDropEventArgs e)
        {
            if (sender == this.View)
            {
                this.WorkOrders.Add(e.DragDropData as WorkOrder);
            }
        }
    }
}
