﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Shapes;

namespace AdventureWorks.WorkOrders.DragAndDrop
{
    public class DragDropHelper
    {
        public const string DataFormat = "WorkOrder";

        // TODO: Ex1. Task1. Implement Drag Handling in an Attached Behavior. Attached Property.

        // TODO: Ex1. Task2. Implement Drop Handling in an Attached Behavior. Attached Property.

        // TODO: Ex1. Task2. Implement Drop Handling in an Attached Behavior. Dropped Event.

        private static readonly DragDropHelper instance = new DragDropHelper();
        private FrameworkElement dragSource;
        private object dragData;
        private Point dragStart;
        private Window dragWindow;

        static DragDropHelper()
        {
        }

        private DragDropHelper()
        {
        }

        public static DragDropHelper Instance
        {
            get { return instance; }
        }

        // TODO: Ex1. Task1. Implement Drag Handling in an Attached Behavior. Get/Set Accessors.

        // TODO: Ex1. Task1. Implement Drag Handling in an Attached Behavior. IsDragSourceChanged.

        // TODO: Ex1. Task1. Implement Drag Handling in an Attached Behavior. Event Handlers.

        // TODO: Ex1. Task2. Implement Drop Handling in an Attached Behavior. Get/Set Accessors.

        // TODO: Ex1. Task2. Implement Drop Handling in an Attached Behavior. IsDropTargetChanged.

        // TODO: Ex1. Task2. Implement Drop Handling in an Attached Behavior. Event Handlers.

        // TODO: Ex1. Task3. Implement Drag-and-Drop Visual Feedback in an Attached Behavior.
    }
}
