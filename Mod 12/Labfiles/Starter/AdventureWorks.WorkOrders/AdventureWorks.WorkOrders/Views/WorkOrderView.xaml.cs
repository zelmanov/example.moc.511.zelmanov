﻿using System.Windows.Controls;

namespace AdventureWorks.WorkOrders.Views
{
    /// <summary>
    /// Interaction logic for WorkOrderView.xaml
    /// </summary>
    public partial class WorkOrderView : UserControl
    {
        public WorkOrderView()
        {
            InitializeComponent();
        }
    }
}
